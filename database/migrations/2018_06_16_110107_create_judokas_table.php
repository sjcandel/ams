<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJudokasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('judokas', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('status')->default(1);
            $table->string('nom');
            $table->string('prenom');
            $table->string('photo')->default('photo.png');
            $table->date('date_naissance');
            $table->string('dojo');
            $table->string('grade');
            $table->string('licence');
            $table->string('telephone')->nullable();
            $table->string('portable')->nullable();
            $table->string('email')->nullable();
            $table->string('adresse_rue')->nullable();
            $table->string('adresse_cp')->nullable();
            $table->string('adresse_ville')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('judokas');
    }
}
