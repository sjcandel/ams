<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCalendriersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calendriers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nom');
            $table->string('niveau');
            $table->string('categorie');
            $table->string('lieu');
            $table->date('date');
            $table->text('horaires');
            $table->boolean('is_selection')->default(0);
            $table->boolean('is_qualificatif')->default(0);
            $table->integer('auteur')->unsigned()->nullable();
            $table->foreign('auteur')->references('id')->on('users')->onDelete('cascade');
            $table->boolean('admin_validation')->default(0);
            $table->dateTime('published_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calendriers');
    }
}
