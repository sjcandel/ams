<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActualitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('actualites', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug')->unique()->nullable();
            $table->integer('auteur')->unsigned()->nullable();
            $table->foreign('auteur')->references('id')->on('users')->onDelete('cascade');
            // $table->string('auteur');
            $table->string('title');
            $table->text('content');
            $table->string('image')->nullable();
            $table->boolean('status')->default(0);
            $table->boolean('admin_validation')->default(0);
            $table->dateTime('published_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('actualites');
    }
}
