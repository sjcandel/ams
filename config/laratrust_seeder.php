<?php

return [
    'role_structure' => [
        'superadministrator' => [
            'acl' => 'c,r,u,d',
            'profile' => 'r,u',
            'actualites' => 'c,r,u,d',
            'calendrier' => 'c,r,u,d',
            'resultats' => 'c,r,u,d',
            'galerie' => 'c,r,u,d',
            'judokas' => 'c,r,u,d',
            'frais' => 'c,r,u,d',
            'utilisateurs' => 'c,r,u,d',
            'roles' => 'c,r,u,d',
            'permissions' => 'c,r,u,d'
        ],
        'administrator' => [
            'profile' => 'r,u',
            'actualites' => 'c,r,u',
            'calendrier' => 'c,r,u',
            'resultats' => 'c,r,u',
            'galerie' => 'c,r,u',
            'judokas' => 'c,r,u',
            'frais' => 'c,r,u',
            'utilisateurs' => 'r'
        ],
        'moderator' => [
            'profile' => 'r,u',
            'actualites' => 'c,r,u',
            'calendrier' => 'c,r,u',
            'resultats' => 'c,r,u',
            'galerie' => 'c,r,u',
            'judokas' => 'c,r,u',
            'frais' => 'c,r,u',
        ],
        'secretary' => [
            'profile' => 'r,u',
            'actualites' => 'c,r,u',
            'calendrier' => 'c,r,u',
            'resultats' => 'c,r,u',
            'galerie' => 'c,r,u',
            'judokas' => 'c,r,u',
            'frais' => 'r',
        ],
        'trainer' => [
            'profile' => 'r,u',
            'actualites' => 'c,r,u',
            'calendrier' => 'c,r,u',
            'resultats' => 'c,r,u',
            'galerie' => 'c,r,u',
            'judokas' => 'r',
            'frais' => 'c,r,u',
        ],
        'accountant' => [
            'profile' => 'r,u',
            'actualites' => 'c,r,u',
            'calendrier' => 'r',
            'resultats' => 'r',
            'galerie' => 'c,r,u',
            'judokas' => 'r',
            'frais' => 'c,r,u',
        ],
        'contributor' => [
            'profile' => 'r,u',
            'actualites' => 'c,r,u',
            'calendrier' => 'c,r,u',
            'resultats' => 'c,r,u',
            'galerie' => 'c,r,u',
        ],
        'guest' => [
            'profile' => 'r',
            'actualites' => 'r',
            'calendrier' => 'r',
            'resultats' => 'r',
            'galerie' => 'r',
        ],
    ],
    'permission_structure' => [],
    // 'permission_structure' => [
    //     'cru_user' => [
    //         'profile' => 'c,r,u'
    //     ],
    // ],
    'permissions_map' => [
        'c' => 'create',
        'r' => 'read',
        'u' => 'update',
        'd' => 'delete'
    ]
];
