@extends('layouts.layouts-intranet.master') 
@section('content')
<div class="flex-container">
  <div class="columns m-t-10">
    <div class="column">
      <h1 class="title">
        <a href="{{route('permissions.index')}}" class="button is-outlined m-r-10">
                <span class="icon">
                    <i class="fas fa-chevron-left"></i>
                </span>
              </a>Détails</h1>
    </div>
    <!-- end of column -->

    <div class="column">
      <a href="{{route('permissions.edit', $permission->id)}}" class="button is-primary is-pulled-right"><i class="fa fa-edit m-r-10"></i> Edit Permission</a>
    </div>
  </div>
  <hr class="m-t-0">

  <div class="columns">
    <div class="column">
      <div class="box">
        <article class="media">
          <div class="media-content">
            <div class="content">
              <p>
                <strong>{{$permission->display_name}}</strong> <small>{{$permission->name}}</small>
                <br> {{$permission->description}}
              </p>
            </div>
          </div>
        </article>
      </div>
    </div>
  </div>
</div>
@endsection