@extends('layouts.layouts-intranet.master') 
@section('content')

<div class="columns">
    <div class="column">
        <h1 class="title">Roles</h1>
    </div>
    <div class="column has-text-right">
        <a href="{{ route('roles.create') }}" class="button is-primary has-text-white is-size-5"><i class="fas fa-plus"></i></a>
    </div>
</div>
<hr>

<div class="columns is-multiline">
    @foreach ($roles as $role)
    <div class="column is-one-quarter">
        <div class="box">
            <article class="media">
                <div class="media-content has-text-centered">

                    <p class="is-size-5"><b>{{$role->display_name}}</b></p>
                    <p>{{$role->description}}</p>

                    <hr>
                    <div>
                        <a href="{{route('roles.show', $role->id)}}" class="button is-outlined">
                            <span class="icon">
                            <i class="far fa-eye"></i>
                            </span>
                        </a>
                        <a href="{{route('roles.edit', $role->id)}}" class="button is-outlined is-hidden-mobile">
                            <span class="icon">
                            <i class="fas fa-pencil-alt"></i>
                            </span>
                        </a>
                    </div>

                </div>
            </article>
        </div>
    </div>
    @endforeach
</div>
@endsection