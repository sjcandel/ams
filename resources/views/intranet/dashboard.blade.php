@extends('layouts.layouts-intranet.master') 
@section('content')


<h1 class="title">Tableau de bord</h1>

<hr>

<div class="columns">
    <div class="column">
        <div class="box">
            <div class="media">
                <div class="media-content has-text-centered">
                    <i class="fas fa-newspaper is-size-3"></i>
                    <p class="is-size-5"><b>{{$actualites}} actualité(s)</b></p>
                    <p>partagées</p>
                    {{-- <p>partagées <span class="has-text-success is-size-7"> (<i class="fas fa-long-arrow-alt-up"></i> 15)</span></p> --}}
                </div>
            </div>
        </div>
    </div>
    <div class="column">
        <div class="box">
            <div class="media">
                <div class="media-content has-text-centered">
                    <i class="fas fa-calendar-alt is-size-3"></i>
                    <p class="is-size-5"><b>{{$calendrier}} compétition(s)</b></p>
                    <p>à venir</p>
                    {{-- <p>à venir<span class="has-text-danger is-size-7"> (<i class="fas fa-long-arrow-alt-down"></i> 3)</span></p> --}}
                </div>
            </div>
        </div>
    </div>
    <div class="column">
        <div class="box">
            <div class="media">
                <div class="media-content has-text-centered">
                    <i class="fas fa-trophy is-size-3"></i>
                    <p class="is-size-5"><b>0 résultat(s)</b></p>
                    <p>réalisés par nos judokas</p>
                    {{-- <p>réalisés par nos judokas <span class="has-text-success is-size-7"> (<i class="fas fa-long-arrow-alt-up"></i> 6)</span></p> --}}
                </div>
            </div>
        </div>
    </div>
    <div class="column">
        <div class="box">
            <div class="media">
                <div class="media-content has-text-centered">
                    <i class="fas fa-camera is-size-3"></i>
                    <p class="is-size-5"><b>{{$photos}} photo(s)</b></p>
                    <p>mises en ligne</p>
                    {{-- <p>mises en ligne <span class="has-text-danger is-size-7"> (<i class="fas fa-long-arrow-alt-down"></i> 4)</span></p> --}}
                </div>
            </div>
        </div>
    </div>
</div>

</div>
@endsection