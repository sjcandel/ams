@extends('layouts.layouts-intranet.master') 
@section('content')

<div class="columns is-mobile">
  <div class="column">
    <h1 class="title">Résultats</h1>
  </div>
  <div class="column has-text-right is-one-quarter">
      @if (Laratrust::can('create-resultats'))
    <a href="{{ route('resultats.create') }}" class="button is-primary has-text-white is-size-5"><i class="fas fa-plus"></i></a>@endif
  </div>
</div>
<hr>

<div class="card">
  <div class="card-content">
    ...
  </div>
</div>
@endsection
 
@section('scripts')
@endsection