@extends('layouts.layouts-intranet.master') 
@section('content')

<div class="columns is-mobile">
  <div class="column">
    <h1 class="title">
      <a href="{{route('frais.index')}}" class="button is-outlined m-r-10">
        <span class="icon">
          <i class="fas fa-chevron-left"></i>
        </span>
      </a>Fiches de frais - create
    </h1>
  </div>
</div>
<hr>

<div class="card">
  <div class="card-content">
    ...
  </div>
</div>
@endsection
 
@section('scripts')
@endsection