@extends('layouts.layouts-intranet.master') 
@section('content')

<div class="columns is-mobile">
  <div class="column">
      <h1 class="title">
          <a href="{{route('galerie.index')}}" class="button is-outlined m-r-10">
            <span class="icon">
              <i class="fas fa-chevron-left"></i>
            </span>
          </a>Nouvel album
        </h1>
  </div>
</div>
<hr> @if($errors->any())
<article class="message is-danger">
  <div class="message-header">
    <span>
      <i class="fas fa-exclamation-triangle"></i> Attention
    </span>
  </div>
  <div class="message-body">
    @foreach($errors->all() as $error)
    <p>{{$error}}</p>
    @endforeach
  </div>
</article>
@endif

<form action="{{route('galerie.store')}}" method="POST" enctype="multipart/form-data">
  {{ csrf_field() }}

  <div class="columns">
    <div class="column is-three-quarters">
      <div class="card">
        <div class="card-content">
          <div class="field">
            <label for="titre" class="label">Titre de l'album <span><sup class="has-text-danger">*</sup></span></label>
            <p class="control">
              <input type="text" class="input" name="titre" id="titre">
            </p>
          </div>
          <div class="field m-b-20">
            <label for="lieu" class="label">Lieu <span><sup class="has-text-danger">*</sup></span></label>
            <p class="control">
                <input type="text" class="input" name="lieu" id="lieu">
            </p>
          </div>
          <div class="field m-b-20">
            <label for="date" class="label">Date <span><sup class="has-text-danger">*</sup></span></label>
            <p class="control">
                <input type="date" class="input" name="date" id="date">
            </p>
          </div>
          
        </div>
      </div>
    </div>
    <div class="column">
      <div class="card">
        <div class="card-content">
          <label for="photo" class="label">Photos</label>
          <input type="file" name="files[]" multiple>
          {{-- <b-field>
              <b-upload v-model="dropFiles"
                  multiple
                  drag-drop>
                  <section class="section">
                      <div class="content has-text-centered">
                          <p>
                              <b-icon
                                  icon="upload"
                                  size="is-large">
                              </b-icon>
                          </p>
                          <p>Importer des photos</p>
                      </div>
                  </section>
              </b-upload>
          </b-field> --}}
  
          {{-- <div class="tags">
              <span v-for="(photo, index) in dropFiles"
                  :key="index"
                  class="tag is-primary" >
                  {{photo.name}}
                  <button class="delete is-small"
                      type="button"
                      @click="deleteDropFile(index)">
                  </button>
              </span>
          </div> --}}
        </div>

      </div>
    </div>
  </div>

 
      <div class="columns">
        
        <div class="column is-three-quarters field is-grouped">
          <p class="control">
            <a href="{{route('galerie.index')}}" class="button is-danger  is-fullwidth">
              <span class="icon is-small">
                <i class="fas fa-ban"></i>
              </span>
              <span>Annuler</span>
            </a>
          </p>
          <p class="control">
            <button class="button is-success is-fullwidth">
              <span class="icon is-small">
                <i class="fas fa-check"></i>
              </span>
              <span>Enregistrer</span>
            </button>
          </p>
        </div>
      </div>

</form>
@endsection
 
@section('scripts')
<script>
    var app = new Vue({
        el: '#app',
        data: {
            dropFiles: []
        },
        methods: {
          deleteDropFile(index) {
                this.dropFiles.splice(index, 1)
            }
        }
})

</script>
@endsection