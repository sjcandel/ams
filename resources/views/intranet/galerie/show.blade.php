@extends('layouts.layouts-intranet.master') 
@section('content')

<div class="columns is-mobile">
    <div class="column">
        <h1 class="title">
            <a href="{{route('galerie.index')}}" class="button is-outlined m-r-10">
                <span class="icon">
                    <i class="fas fa-chevron-left"></i>
                </span>
            </a> Détails
        </h1>
    </div>
    <div class="column has-text-right">
        @if (Laratrust::can('update-galerie'))
        <a href="{{route('galerie.edit', $album->id)}}" class="button is-pulled-right m-l-10">
          <span class="icon is-small">
              <i class="fas fa-pencil-alt"></i>
          </span>
          <span>Modifier</span>
        </a>@endif
    </div>
</div>
<hr>

<div class="columns">
  <div class="column is-one-quarter">
    <div class="card">
        <div class="card-content">
          <div class="field">
            <label for="titre" class="label">Titre de l'album</label>
            <p class="pre">{{$album->titre}}</p>
          </div>
          <div class="field">
            <label for="lieu" class="label">Lieu</label>
            <p class="pre">{{$album->lieu}}</p>
          </div>
          <div class="field">
            <label for="date" class="label">Date</label>
            <p class="pre">{{ \Carbon\Carbon::parse($album->date)->format('d/m/Y')}}</p>
          </div>
        </div>
      </div>
  </div>
  

  <div class="column">
      <div class="card">
          <div class="card-content">
            <div class="columns">
                @foreach($photos as $photo)
                    <div class="column is-one-third has-background-white-bis">
                        <figure class="image is-5by4 avatar">
                            <img src="{{asset('uploads/assets/photos/galerie/'.$photo->nom)}}">
                        </figure>
                    </div>
                @endforeach
            </div>
          </div>
        </div>
  </div>

</div>

      
  
@endsection
 
@section('scripts')
@endsection