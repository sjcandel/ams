@extends('layouts.layouts-intranet.master') 
@section('content')

<div class="columns is-mobile">
  <div class="column">
    <h1 class="title">Galerie</h1>
  </div>
  <div class="column has-text-right is-one-quarter">
      @if (Laratrust::can('create-galerie'))
    <a href="{{ route('galerie.create') }}" class="button is-primary has-text-white is-size-5"><i class="fas fa-plus"></i></a>@endif
  </div>
</div>
<hr>

<div class="card" v-if="tab == 'published'">
    <div class="card-content">
  
      <table class="table is-hoverable is-fullwidth">
        <thead>
          <tr>
            <th>Titre de l'album</th>
            <th class="is-hidden-mobile">Lieu</th>
            <th class="is-hidden-mobile">Date</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          @foreach ($albums as $album)
          <tr>
            <td>{{$album->titre}}</td>
            <td class="is-hidden-mobile">{{$album->lieu}}</td>
            <td class="is-hidden-mobile">{{\Carbon\Carbon::parse($album->date)->format('d/m/Y')}}</td>
  
            <td class="has-text-right">
                @if (Laratrust::can('read-galerie'))
                <a href="{{route('galerie.show', $album->id)}}" class="button is-outlined">
                    <span class="icon">
                      <i class="far fa-eye"></i>
                    </span>
                  </a>@endif
                @if (Laratrust::can('update-galerie'))
                <a href="{{route('galerie.edit', $album->id)}}" class="button is-outlined">
                    <span class="icon">
                      <i class="fas fa-pencil-alt"></i>
                    </span>
                  </a>@endif
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
@endsection
 
@section('scripts')
@endsection