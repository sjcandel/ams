<style>
    /* <?php include(public_path().'/css/app.css');
    ?> */

    .container {
        padding: 2cm;
    }

    .title {
        font-size: 1.5rem;
        font-weight: 600;
        line-height: 1.125;
        margin-bottom: 2rem;
    }

    .float-right {
        float: right;
    }

    .float-left {
        float: left;
    }

    .pre {
        background-color: whitesmoke;
        padding: 0.5em;
        margin-top: 10px;
    }

    hr {
        height: 1px;
        background-color: #dbdbdb;
        border: none;
        display: block;
        margin: 30px 0;
    }

    .avatar {
        position: relative;
    }

    .avatar img {
        width: 100%;
        height: 100%;
        -o-object-fit: cover;
        object-fit: cover;
        position: absolute;
        top: 0;
        bottom: 0;
        right: 0;
        left: 0;
    }

    .avatar img:before {
        display: block;
        content: "";
        width: 100%;
        padding-top: calc((1 / 1) * 100%);
    }
</style>

<div class="container">


    <div class="float-right" style="width: 70%; margin-left: 5%;">
        <h1 class="title">{{ $judoka->nom }} {{ $judoka->prenom }}</h1>
        <p><b>Date de naissance</b></p>
        <p class="pre">{{ $judoka->date_naissance }}</p>
        <hr>
        <div class="float-left" style="width: 40%;">
            <p><b>Licence</b></p>
            <p class="pre">{{ $judoka->licence }}</p>
        </div>
        <div class="float-left" style="width: 20%; margin-left: 42%;">
            <p><b>Dojo</b></p>
            <p class="pre">{{ $judoka->dojo }}</p>
        </div>
        <div style="width: 37%; margin-left: 64%;">
            <p><b>Ceinture</b></p>
            <p class="pre">{{ $judoka->grade }}</p>
        </div>
        <hr>
        <div class="float-left" style="width: 49%;">
            <p><b>Téléphone</b></p>
            <p class="pre">{{ $judoka->telephone }}</p>
        </div>
        <div style="width: 49%; margin-left: 51%;">
            <p><b>Portable</b></p>
            <p class="pre">{{ $judoka->portable }}</p>
        </div>
        <div style="margin-top: 20px;">
            <p><b>Email</b></p>
            <p class="pre">{{ $judoka->email }}</p>
        </div>
        <div style="margin-top: 20px;">
            <p><b>Adresse</b></p>
            <p class="pre">{{ $judoka->adresse_rue }}</p>
        </div>

        <div class="float-left" style="margin-top: 20px; width: 20%">
            <p><b>CP</b></p>
            <p class="pre">{{ $judoka->adresse_cp }}</p>
        </div>
        <div style="margin-left: 22%; width: 78%">
            <p><b>Ville</b></p>
            <p class="pre">{{ $judoka->adresse_ville }}</p>
        </div>
        <hr>

    </div>
    <div style="width: 25%;">
        <div class="avatar">
            <img src="{{ base_path('storage/app/public/assets/photos/judokas/' . $judoka->photo) }}" />
        </div>
    </div>

    <div style="clear:both"></div>

</div>