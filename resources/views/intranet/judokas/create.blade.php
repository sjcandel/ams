@extends('layouts.layouts-intranet.master') 
@section('content')

<div class="columns is-mobile">
    <div class="column">
        <h1 class="title">
            <a href="{{route('judokas.index')}}" class="button is-outlined m-r-10">
                <span class="icon">
                    <i class="fas fa-chevron-left"></i>
                </span>
            </a>Nouveau licencié
        </h1>
    </div>
</div>

<hr> @if($errors->any())
<article class="message is-danger">
    <div class="message-header">
        <span><i class="fas fa-exclamation-triangle"></i> Attention</span>
    </div>
    <div class="message-body">
        @foreach($errors->all() as $error)
        <p>{{$error}}</p>
        @endforeach
    </div>
</article>
@endif

<form action="{{route('judokas.store')}}" method="POST" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="columns">
        <div class="column">
            <div class="card">
                <header class="card-header">
                    <p class="card-header-title">
                        Informations générales
                    </p>
                </header>
                <div class="card-content">
                    <div class="columns">
                        <div class="column">
                            <div class="field">
                                <label for="nom" class="label">Nom <span><sup class="has-text-danger">*</sup></span></label>
                                <p class="control">
                                    <input type="text" class="input" name="nom" id="nom">
                                </p>
                            </div>
                        </div>
                        <div class="column">
                            <div class="field">
                                <label for="prenom" class="label">Prénom <span><sup class="has-text-danger">*</sup></span></label>
                                <p class="control">
                                    <input type="text" class="input" name="prenom" id="prenom">
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="field">
                        <label for="date_naissance" class="label">Date de naissance <span><sup class="has-text-danger">*</sup></span></label>
                        <p class="control">
                            <input type="date" class="input" name="date_naissance" id="date_naissance">
                        </p>
                    </div>
                </div>
            </div>
            <div class="card m-t-20">
                <header class="card-header">
                    <p class="card-header-title">
                        Informations club
                    </p>
                </header>
                <div class="card-content">
                    <div class="columns">
                        <div class="column is-three-fifths">
                            <div class="field">
                                <label for="licence" class="label">Licence <span><sup class="has-text-danger">*</sup></span></label>
                                <p class="control">
                                    <input type="text" class="input" name="licence" id="licence">
                                </p>
                            </div>
                        </div>
                        <div class="column is-narrow">
                            <div class="field">
                                <label for="dojo" class="label">Dojo <span><sup class="has-text-danger">*</sup></span></label>
                                <div class="select">
                                    <select name="dojo" id="dojo">
                                        <option value="A">A</option>
                                        <option value="B">B</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="column is-narrow">
                            <div class="field">
                                <label for="grade" class="label">Ceinture <span><sup class="has-text-danger">*</sup></span></label>
                                <div class="select">
                                    <select name="grade" id="grade">
                                        <option value="Blanche">Blanche</option>
                                        <option value="Blanche-jaune">Blanche-jaune</option>
                                        <option value="Jaune">Jaune</option>
                                        <option value="Jaune-orange">Jaune-orange</option>
                                        <option value="Orange">Orange</option>
                                        <option value="Orange-verte">Orange-verte</option>
                                        <option value="Verte">Verte</option>
                                        <option value="Verte-bleue">Verte-bleue</option>
                                        <option value="Bleue">Bleue</option>
                                        <option value="Marron">Marron</option>
                                        <option value="" disabled></option>
                                        <option value="1er Dan">1er Dan</option>
                                        <option value="2eme Dan">2eme Dan</option>
                                        <option value="2eme Dan">3eme Dan</option>
                                        <option value="4eme Dan">4eme Dan</option>
                                        <option value="5eme Dan">5eme Dan</option>
                                        <option value="6eme Dan">6eme Dan</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card m-t-20">
                <header class="card-header">
                    <p class="card-header-title">
                        Contact
                    </p>
                </header>
                <div class="card-content">
                    <div class="columns">
                        <div class="column">
                            <div class="field">
                                <label for="telephone" class="label">Telephone</label>
                                <p class="control">
                                    <input type="text" class="input" name="telephone" id="telephone">
                                </p>
                            </div>
                        </div>
                        <div class="column">
                            <div class="field">
                                <label for="portable" class="label">Portable</label>
                                <p class="control">
                                    <input type="text" class="input" name="portable" id="portable">
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="field">
                        <label for="email" class="label">Email</label>
                        <p class="control">
                            <input type="text" class="input" name="email" id="email">
                        </p>
                    </div>

                    <div class="field">
                        <label for="adresse_rue" class="label">Adresse</label>
                        <p class="control">
                            <input type="text" class="input" name="adresse_rue" id="adresse_rue">
                        </p>
                    </div>
                    <div class="columns">
                        <div class="column is-one-third">
                            <div class="field">
                                <label for="adresse_cp" class="label">CP</label>
                                <p class="control">
                                    <input type="text" class="input" name="adresse_cp" id="adresse_cp">
                                </p>
                            </div>
                        </div>
                        <div class="column">
                            <div class="field">
                                <label for="adresse_ville" class="label">Ville</label>
                                <p class="control">
                                    <input type="text" class="input" name="adresse_ville" id="adresse_ville">
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="column is-one-quarter">
            <div class="card">
                <div class="card-content">
                    <div class="file is-boxed" v-if="!image">
                        <label class="file-label" for="file">
                            <input class="file-input" type="file" name="file" id="file" @change="onFileChange">
                            <span class="file-cta">
                                <span class="file-icon">
                                    <i class="fas fa-upload"></i>
                                </span>
                                <span class="file-label">
                                    Importer une photo...
                                </span>
                            </span>
                        </label>
                    </div>
                    <div v-else v-cloak>
                        <figure class="image is-2by3 avatar">
                            <img :src="image">
                        </figure>
                        <div class="file is-centered m-t-20 is-fullwidth">
                            <label class="file-label" for="file">
                                <input class="file-input" type="file" name="file" id="file" @change="onFileChange">
                                <span class="file-cta">
                                    <span class="file-icon">
                                        <i class="fas fa-upload"></i>
                                    </span>
                                    <span class="file-label">
                                        Modifier la photo...
                                    </span>
                                </span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="columns">
        <div class="column">
            <div class="field is-grouped">
                <p class="control">
                    <a href="{{route('judokas.index')}}" class="button is-danger  is-fullwidth">
                    <span class="icon is-small">
                        <i class="fas fa-ban"></i>
                    </span>
                    <span>Annuler</span>
                    </a>
                </p>
                <p class="control">
                    <button class="button is-success is-fullwidth">
                        <span class="icon is-small">
                            <i class="fas fa-check"></i>
                        </span>
                        <span>Enregistrer</span>
                    </button>
                </p>
            </div>
        </div>
        <div class="column is-one-quarter"></div>
    </div>
</form>
@endsection
 
@section('scripts')
<script>
    var app = new Vue({
        el: '#app',
        data: {
            image: ''
        },
        methods: {
            onFileChange(e) {
            var files = e.target.files || e.dataTransfer.files;
            if (!files.length)
                return;
            this.createImage(files[0]);
            },
            createImage(file) {
            var image = new Image();
            var reader = new FileReader();
            var vm = this;

            reader.onload = (e) => {
                vm.image = e.target.result;
            };
            reader.readAsDataURL(file);
            }
        }
})

</script>
@endsection