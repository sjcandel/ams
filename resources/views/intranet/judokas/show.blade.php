@extends('layouts.layouts-intranet.master') 
@section('content')

<div class="columns is-mobile">
    <div class="column">
        <h1 class="title">
            <a href="{{route('judokas.index')}}" class="button is-outlined m-r-10">
                <span class="icon">
                    <i class="fas fa-chevron-left"></i>
                </span>
            </a> Détails
        </h1>
    </div>
    <div class="column has-text-right">
        @if (Laratrust::can('update-judokas'))
        <a href="{{route('judokas.edit', $judoka->id)}}" class="button is-pulled-right m-l-10">
                <span class="icon is-small">
                    <i class="fas fa-pencil-alt"></i>
                </span>
                <span>Modifier</span>
            </a>@endif
        <a href="{{route('judokas.pdf', $judoka->id)}}" class="button is-pulled-right">
            <span class="icon is-small">
                <i class="fas fa-file-pdf"></i>
            </span>
            <span>Fiche PDF</span>
        </a>
    </div>
</div>

<hr>

<div class="columns">
    <div class="column">
        <div class="card">
            <header class="card-header">
                <p class="card-header-title">
                    Informations générales
                </p>
            </header>
            <div class="card-content">
                <div class="columns">
                    <div class="column">
                        <div class="field">
                            <label for="nom" class="label">Nom</label>
                            <p class="pre">{{$judoka->nom}}</p>
                        </div>
                    </div>
                    <div class="column">
                        <div class="field">
                            <label for="prenom" class="label">Prénom</label>
                            <p class="pre">{{$judoka->prenom}}</p>
                        </div>
                    </div>
                </div>
                <div class="field">
                    <label for="date_naissance" class="label">Date de naissance</label>
                    <p class="pre">{{ \Carbon\Carbon::parse($judoka->date_naissance)->format('d/m/Y')}}</p>
                </div>
            </div>
        </div>

        <div class="card m-t-20">
            <header class="card-header">
                <p class="card-header-title">
                    Informations club
                </p>
            </header>
            <div class="card-content">
                <div class="columns">
                    <div class="column">
                        <div class="field">
                            <label for="licence" class="label">Licence</label>
                            <p class="pre">{{$judoka->licence}}</p>
                        </div>
                    </div>
                    <div class="column">
                        <div class="field">
                            <label for="dojo" class="label">Dojo</label>
                            <p class="pre">{{$judoka->dojo}}</p>
                        </div>
                    </div>
                    <div class="column">
                        <div class="field">
                            <label for="grade" class="label">Ceinture</label>
                            <p class="pre">{{$judoka->grade}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="card m-t-20">
            <header class="card-header">
                <p class="card-header-title">
                    Contact
                </p>
            </header>
            <div class="card-content">
                <div class="columns">
                    <div class="column">
                        <div class="field">
                            <label for="telephone" class="label">Telephone</label>
                            <p class="pre"><i class="fas fa-phone m-r-10"></i> <a href="tel:{{$judoka->telephone}}">{{$judoka->telephone}}</a></p>
                        </div>
                    </div>
                    <div class="column">
                        <div class="field">
                            <label for="portable" class="label">Portable</label>
                            <p class="pre"><i class="fas fa-mobile-alt m-r-10"></i> <a href="tel:{{$judoka->portable}}">{{$judoka->portable}}</a></p>
                        </div>
                    </div>
                </div>
                <div class="field">
                    <label for="email" class="label">Email</label>
                    <p class="pre"><i class="fas fa-envelope m-r-10"></i> <a href="mailto:{{$judoka->email}}">{{$judoka->email}}</a></p>
                </div>

                <div class="field">
                    <label for="adresse_rue" class="label">Adresse</label>
                    <p class="pre">{{$judoka->adresse_rue}}</p>
                </div>
                <div class="columns">
                    <div class="column is-one-third">
                        <div class="field">
                            <label for="adresse_cp" class="label">CP</label>
                            <p class="pre">{{$judoka->adresse_cp}}</p>
                        </div>
                    </div>
                    <div class="column">
                        <div class="field">
                            <label for="adresse_ville" class="label">Ville</label>
                            <p class="pre">{{$judoka->adresse_ville}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <hr>
        <div class="card m-t-20">
            <header class="card-header">
                <p class="card-header-title">Résultats en compétition</p>
            </header>
            <div class="card-content"></div>
        </div>
    </div>

    <div class="column is-one-quarter">
        <div class="card">
            <div class="card-content">
                <figure class="image is-2by3 avatar">
                    <img src="{{asset('uploads/assets/photos/judokas/'.$judoka->photo) }}">
                </figure>
            </div>
        </div>
    </div>
</div>
@endsection