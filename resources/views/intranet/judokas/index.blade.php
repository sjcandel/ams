@extends('layouts.layouts-intranet.master') 
@section('content')

<div class="columns is-mobile">
  <div class="column">
    <h1 class="title">Licenciés</h1>
  </div>
  <div class="column has-text-right is-one-quarter">
    @if (Laratrust::can('create-judokas'))
    <a href="{{ route('judokas.create') }}" class="button is-primary has-text-white is-size-5"><i class="fas fa-plus"></i></a>@endif
  </div>
</div>

<div class="tabs">
  <ul>
    <li v-bind:class="{ 'is-active': tab == 'current' }" v-on:click="tab = 'current'">
      <a>
        <span class="icon is-small "><i class="far fa-calendar-alt"></i></span>
        <span>Année 2017-2018 <span class="tag is-rounded is-white"><b class="has-text-grey">{{$judokasCount}}</b></span></span>
      </a>
    </li>
    <li v-bind:class="{ 'is-active': tab == 'archive' }" v-on:click="tab = 'archive'">
      <a>
        <span class="icon is-small "><i class="fas fa-archive"></i></span>
        <span>Archives <span class="tag is-rounded is-white"><b class="has-text-grey">{{$archivesCount}}</b></span></span>
      </a>
    </li>
  </ul>
</div>

<div v-if="tab == 'current'">
  <div class="m-t-40 m-b-40">
    <div class="columns">
      <div class="column">
        <div class="field">
          <p class="control has-icons-left">
            <input class="input" type="text" placeholder="Rechercher ...">
            <span class="icon is-small is-left">
              <i class="fas fa-search"></i>
            </span>
          </p>
        </div>
      </div>
      <div class="column is-narrow">
        <div class="field">
          <div class="select">
            <select name="dojo" id="dojo">
                  <option selected disabled>Dojo</option>
                  <option value="All">Tous</option>
                  <option value="A">A</option>
                  <option value="B">B</option>
                </select>
          </div>
        </div>
      </div>
      <div class="column is-narrow">
        <div class="field">
          <div class="select">
            <select name="dojo" id="dojo">
                  <option selected disabled>Catégorie d'âge</option>
                  <option value="All">Toutes</option>
                  <option value="poussin">poussin</option>
                  <option value="benjamin">benjamin</option>
                  <option value="minime">minime</option>
                  <option value="cadet">cadet</option>
                  <option value="junior">junior</option>
                  <option value="senior">senior</option>
                  <option value="veteran">veteran</option>
                </select>
          </div>
        </div>
      </div>
      <div class="column"></div>
    </div>
  </div>

  <div class="card">
    <div class="card-content">
      <table class="table is-hoverable is-fullwidth ">
        <thead>
          <tr>
            <th></th>
            <th>Nom</th>
            <th>Prénom</th>
            <th class="is-hidden-mobile ">Date de naissance</th>
            <th class="is-hidden-mobile ">Licence</th>
            <th class="is-hidden-mobile ">Dojo</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          @foreach ($judokas as $judoka)
          <tr>
            <td>
              <div class="avatar-round "><img src="{{asset( 'uploads/assets/photos/judokas/'.$judoka->photo) }}"></div>
            </td>
            <td>{{$judoka->nom}}</td>
            <td>{{$judoka->prenom}}</td>
            <td class="is-hidden-mobile">{{ \Carbon\Carbon::parse($judoka->date_naissance)->format('d/m/Y')}}</td>
            <td class="is-hidden-mobile">{{$judoka->licence}}</td>
            <td class="is-hidden-mobile">{{$judoka->dojo}}</td>
            <td class="has-text-right">
              <a href="{{route('judokas.show', $judoka->id)}}" class="button is-outlined">
                <span class="icon">
                  <i class="far fa-eye"></i>
                </span>
              </a> @if (Laratrust::can('update-judokas'))
              <a href="{{route('judokas.edit', $judoka->id)}}" class="button is-outlined is-hidden-mobile">
                <span class="icon">
                  <i class="fas fa-pencil-alt"></i>
                </span>
              </a> @endif

            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>

<div v-if="tab == 'archive'" v-cloak>
  <div class="m-t-40 m-b-40">
    <div class="columns">
      <div class="column">
        <div class="field">
          <p class="control has-icons-left">
            <input class="input" type="text" placeholder="Rechercher ...">
            <span class="icon is-small is-left">
                  <i class="fas fa-search"></i>
                </span>
          </p>
        </div>
      </div>
      <div class="column is-narrow">
        <div class="field">
          <div class="select">
            <select name="dojo" id="dojo">
                      <option selected disabled>Dojo</option>
                      <option value="All">Tous</option>
                      <option value="A">A</option>
                      <option value="B">B</option>
                    </select>
          </div>
        </div>
      </div>
      <div class="column is-narrow">
        <div class="field">
          <div class="select">
            <select name="dojo" id="dojo">
                      <option selected disabled>Catégorie d'âge</option>
                      <option value="All">Toutes</option>
                      <option value="poussin">poussin</option>
                      <option value="benjamin">benjamin</option>
                      <option value="minime">minime</option>
                      <option value="cadet">cadet</option>
                      <option value="junior">junior</option>
                      <option value="senior">senior</option>
                      <option value="veteran">veteran</option>
                    </select>
          </div>
        </div>
      </div>
      <div class="column"></div>
    </div>
  </div>

  <div class="card">
    <div class="card-content">
      <table class="table is-hoverable is-fullwidth ">
        <thead>
          <tr>
            <th></th>
            <th>Nom</th>
            <th>Prénom</th>
            <th class="is-hidden-mobile ">Date de naissance</th>
            <th class="is-hidden-mobile ">Licence</th>
            <th class="is-hidden-mobile ">Dojo</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          @foreach ($archives as $archive)
          <tr>
            <td>
              <div class="avatar-round "><img src="{{asset( 'uploads/assets/photos/judokas/'.$archive->photo) }}"></div>
            </td>
            <td>{{$archive->nom}}</td>
            <td>{{$archive->prenom}}</td>
            <td class="is-hidden-mobile">{{ \Carbon\Carbon::parse($archive->date_naissance)->format('d/m/Y')}}</td>
            <td class="is-hidden-mobile">{{$archive->licence}}</td>
            <td class="is-hidden-mobile">{{$archive->dojo}}</td>
            <td class="has-text-right">
              <a href="{{route('judokas.show', $archive->id)}}" class="button is-outlined">
                    <span class="icon">
                      <i class="far fa-eye"></i>
                    </span>
                  </a> @if (Laratrust::can('update-judokas'))

              <a href="{{route('judokas.unarchive', $archive->id)}}" class="button is-outlined is-success m-l-20">
                <span class="icon">
                    <i class="fas fa-sync-alt"></i>
                </span>
              </a> @endif

            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>

{{-- {{$judokas->links('vendor.pagination.default')}} --}}
@endsection
 
@section('scripts')
<script>
  var app = new Vue({
    el: '#app',
    data: {
      tab: 'current'
    }
  });

</script>
@endsection