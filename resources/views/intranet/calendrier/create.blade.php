@extends('layouts.layouts-intranet.master') 
@section('content')

<div class="columns is-mobile">
  <div class="column">
    <h1 class="title">
      <a href="{{route('calendrier.index')}}" class="button is-outlined m-r-10">
        <span class="icon">
            <i class="fas fa-chevron-left"></i>
        </span>
      </a>Nouvel événement
    </h1>
  </div>
</div>
<hr>@if($errors->any())
<article class="message is-danger">
  <div class="message-header">
    <span>
      <i class="fas fa-exclamation-triangle"></i> Attention
    </span>
  </div>
  <div class="message-body">
    @foreach($errors->all() as $error)
    <p>{{$error}}</p>
    @endforeach
  </div>
</article>
@endif

<form action="{{route('calendrier.store')}}" method="POST">
  {{ csrf_field() }}

  <div class="columns">
    <div class="column">
      <div class="card">
        <div class="card-content">
          <div class="field">
            <label for="nom" class="label">Nom <span><sup class="has-text-danger">*</sup></span></label>
            <p class="control">
              <input type="text" class="input" name="nom" id="nom">
            </p>
          </div>
          <div class="columns">
            <div class="field column">
              <label for="lieu" class="label">Lieu <span><sup class="has-text-danger">*</sup></span></label>
              <p class="control">
                <input type="text" class="input" name="lieu" id="lieu">
              </p>
            </div>
            <div class="field column">
              <label for="date" class="label">Date <span><sup class="has-text-danger">*</sup></span></label>
              <p class="control">
                <input type="date" class="input" name="date" id="date">
              </p>
            </div>
          </div>
          {{-- <template>
              <label class="label">Date <span><sup class="has-text-danger">*</sup></span></label>
              <b-field>
                  <b-datepicker
                      placeholder="jj/mm/aaaa">
                  </b-datepicker>
              </b-field>
          </template> --}}


          <div>

          </div>
          <div class="field">
            <label for="horaires" class="label">Description / horaires <span><sup class="has-text-danger">*</sup></span></label>
            <p class="control">
              <textarea class="textarea" name="horaires" id="horaires" rows="10"></textarea>
            </p>
          </div>
        </div>
      </div>
    </div>
    <div class="column is-one-quarter">
      <div class="card">
        <div class="card-content">
          <div class="field">
            <label for="categorie" class="label">Catégorie d'âge <span><sup class="has-text-danger">*</sup></span></label>
            <b-radio v-model="categorie" name="categorie" native-value="minipoussins">
              Mini-poussins
            </b-radio>
            <b-radio v-model="categorie" name="categorie" native-value="poussins">
              Poussins
            </b-radio>
            <b-radio v-model="categorie" name="categorie" native-value="benjamins">
              Benjamins
            </b-radio>
            <b-radio v-model="categorie" name="categorie" native-value="minimes">
              Minimes
            </b-radio>
            <b-radio v-model="categorie" name="categorie" native-value="cadets">
              Cadets
            </b-radio>
            <b-radio v-model="categorie" name="categorie" native-value="juniors">
              Juniors
            </b-radio>
            <b-radio v-model="categorie" name="categorie" native-value="seniors">
              Seniors
            </b-radio>
            <b-radio v-model="categorie" name="categorie" native-value="veterans">
              Veterans
            </b-radio>
          </div>
          <hr>
          <div class="field">
            <label for="niveau" class="label">Niveau <span><sup class="has-text-danger">*</sup></span></label>
            <div class="select is-fullwidth">
              <select name="niveau" id="niveau">
                  <option value="district">District</option>
                  <option value="départemental">départemental</option>
                  <option value="régional">régional</option>
                  <option value="interrégional">interrégional</option>
                  <option value="national">national</option>
                  <option value="international">international</option>
              </select>
            </div>
          </div>
          <hr>
          <div class="m-t-20">
            <label for="is_selection">Participation sur sélection :</label>
            <b-radio v-model="isSelection" name="is_selection" native-value="0">Non</b-radio>
            <b-radio v-model="isSelection" name="is_selection" native-value="1" type="is-success">Oui</b-radio>
          </div>
          <div class="m-t-20">
            <label for="is_qualificatif">Compétition qualificative :</label>
            <b-radio v-model="isQualificatif" name="is_qualificatif" native-value="0">Non</b-radio>
            <b-radio v-model="isQualificatif" name="is_qualificatif" native-value="1" type="is-success">Oui</b-radio>
          </div>
        </div>

      </div>
    </div>
  </div>

  <div class="columns">
    <div class="column">
      <div class="field is-grouped">
        <p class="control">
          <a href="{{route('calendrier.index')}}" class="button is-danger  is-fullwidth">
            <span class="icon is-small">
              <i class="fas fa-ban"></i>
            </span>
            <span>Annuler</span>
          </a>
        </p>
        <p class="control">
          <button class="button is-success is-fullwidth">
            <span class="icon is-small">
              <i class="fas fa-check"></i>
            </span>
            <span>Enregistrer</span>
          </button>
        </p>
      </div>
    </div>
    <div class="column is-one-quarter"></div>
  </div>

</form>
@endsection
 
@section('scripts')
<script>
  var app = new Vue({
    el: '#app',
    data: {
      isSelection: 0,
      isQualificatif: 0,
      categorie: []
    },
  });

</script>
@endsection