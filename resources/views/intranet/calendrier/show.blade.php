@extends('layouts.layouts-intranet.master') 
@section('content')

<div class="columns is-mobile">
  <div class="column">
    <h1 class="title">
      <a href="{{route('calendrier.index')}}" class="button is-outlined m-r-10">
        <span class="icon">
            <i class="fas fa-chevron-left"></i>
        </span>
      </a> Détails
    </h1>
  </div>
  <div class="column has-text-right">
    @if (Laratrust::can('update-calendrier'))
    <a href="{{route('calendrier.edit', $evnt->id)}}" class="button is-pulled-right">
          <span class="icon is-small">
              <i class="fas fa-pencil-alt"></i>
          </span>
          <span>Modifier</span>
      </a>@endif
  </div>
</div>
<hr>


<div class="columns">
  <div class="column">
    <div class="card">
      <div class="card-content">
        <div class="field">
          <label for="nom" class="label">Nom</label>
          <p class="pre">{{$evnt->nom}}</p>
        </div>
        <div class="columns">
          <div class="field column">
            <label for="lieu" class="label">Lieu</label>
            <p class="pre">{{$evnt->lieu}}</p>
          </div>
          <div class="field column">
            <label for="date" class="label">Date</label>
            <p class="pre">{{ \Carbon\Carbon::parse($evnt->date)->format('d/m/Y')}}</p>
          </div>
        </div>

        <div>

        </div>
        <div class="field">
          <label for="horaires" class="label">Description / horaires</label>
          <p class="pre">{{$evnt->horaires}}</p>
        </div>
      </div>
    </div>
  </div>
  <div class="column is-one-quarter">
    <div class="card">
      <div class="card-content">
        <div class="field">
          <label for="categorie" class="label">Catégorie(s)</label>
          <p class="pre">{{$evnt->categorie}}</p>
        </div>
        <div class="field">
          <label for="niveau" class="label">Niveau</label>
          <p class="pre">{{$evnt->niveau}}</p>
        </div>
        <hr> @if($evnt->is_selection == 1)
        <span class="tag is-danger is-rounded m-l-5">
          <span class="icon m-r-2"><i class="fas fa-exclamation-circle"></i></span> sur sélection
        </span>
        @endif @if($evnt->is_qualificatif == 1)
        <span class="tag is-success is-rounded m-l-5">
          <span class="icon m-r-2"><i class="fas fa-star"></i></span> qualificatif
        </span>@endif
      </div>

    </div>
  </div>
</div>
@endsection
 
@section('scripts')
@endsection