@extends('layouts.layouts-intranet.master') 
@section('content')

<div class="columns is-mobile">
  <div class="column">
    <h1 class="title">Calendrier</h1>
  </div>
  <div class="column has-text-right is-one-quarter">
    @if (Laratrust::can('create-calendrier'))
    <a href="{{ route('calendrier.create') }}" class="button is-primary has-text-white is-size-5"><i class="fas fa-plus"></i></a>@endif
  </div>
</div>

<div class="tabs">
  <ul>
    <li v-bind:class="{ 'is-active': tab == 'published' }" v-on:click="tab = 'published'">
      <a>
        <span class="icon is-small"><i class="fas fa-edit"></i></span>
        <span>Compétitions à venir <span class="tag is-rounded is-white"><b class="has-text-grey">{{$evenementsCount}}</b></span></span>
      </a>
    </li>
    <li v-bind:class="{ 'is-active': tab == 'archives' }" v-on:click="tab = 'archives'">
      <a>
        <span class="icon is-small"><i class="fas fa-archive"></i></span>
        <span>Compétitions passées <span class="tag is-rounded is-white"><b class="has-text-grey">{{$archivesCount}}</b></span></span>
      </a>
    </li>
    <li v-bind:class="{ 'is-active': tab == 'waiting' }" v-on:click="tab = 'waiting'">
      <a>
        <span class="icon is-small"><i class="fas fa-hourglass-half"></i></i></span>
        <span>En attente<span class="tag is-rounded is-white"><b class="has-text-grey">{{$waitingCount}}</b></span></span>
      </a>
    </li>


  </ul>
</div>

<div v-if="tab == 'published'">

  <div class="m-t-40 m-b-40">
    <div class="columns">
      <div class="column">
        <div class="field">
          <p class="control has-icons-left">
            <input class="input" type="text" placeholder="Rechercher ...">
            <span class="icon is-small is-left">
              <i class="fas fa-search"></i>
            </span>
          </p>
        </div>
      </div>
      <div class="column is-narrow">
        <div class="field">
          <div class="select">
            <select name="niveau" id="niveau">
              <option selected disabled>Niveau</option>
              <option value="district">District</option>
              <option value="départemental">Départemental</option>
              <option value="régional">Régional</option>
              <option value="interrégional">Interrégional</option>
              <option value="national">National</option>
              <option value="international">International</option>
            </select>
          </div>
        </div>
      </div>
      <div class="column is-narrow">
        <div class="field">
          <div class="select">
            <select name="categorie" id="categorie">
              <option selected disabled>Catégorie</option>
              <option value="mini-poussins">Mini-poussins</option>
              <option value="poussins">Poussins</option>
              <option value="minimes">Minimes</option>
              <option value="cadets">Cadets</option>
              <option value="juniors">Juniors</option>
              <option value="seniors">Seniors</option>
              <option value="veterans">Veterans</option>
            </select>
          </div>
        </div>
      </div>

      <div class="column"></div>
    </div>
  </div>


  <div class="card">
    <div class="card-content">
      <table class="table is-hoverable is-fullwidth">
        <thead>
          <tr>
            <th>Nom</th>
            <th class="is-hidden-mobile">Lieu</th>
            <th>Date</th>
            <th class="is-hidden-mobile">Categorie</th>
            <th class="is-hidden-mobile"></th>
            <th class="is-hidden-mobile"></th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          @foreach ($evenements as $evnt)
          <tr>
            <td>{{$evnt->nom}}</td>
            <td class="is-hidden-mobile">{{$evnt->lieu}}</td>
            <td>{{ \Carbon\Carbon::parse($evnt->date)->format('d/m/Y')}}</td>
            <td class="is-hidden-mobile">{{$evnt->categorie}}</td>
            <td class="is-hidden-mobile">
              @if($evnt->is_selection == 1)
              <span class="tag is-danger is-rounded m-l-5">
                <span class="icon m-r-2"><i class="fas fa-exclamation-circle"></i></span> sur sélection
              </span>
              @endif
            </td>

            <td class="is-hidden-mobile">
              @if($evnt->is_qualificatif == 1)
              <span class="tag is-success is-rounded m-l-5">
                <span class="icon m-r-2"><i class="fas fa-star"></i></span> qualificatif
              </span>@endif
            </td>

            <td class="has-text-right">
              <a href="{{route('calendrier.show', $evnt->id)}}" class="button is-outlined">
                <span class="icon">
                  <i class="far fa-eye"></i>
                </span>
              </a> @role('superadministrator|administrator')
              <a href="{{route('calendrier.edit', $evnt->id)}}" class="is-hidden-mobile button is-outlined is-hidden-mobile">
                <span class="icon">
                  <i class="fas fa-pencil-alt"></i>
                </span>
              </a>
              <a href="{{route('calendrier.destroy', $evnt->id)}}" class="is-hidden-mobile button is-outlined is-hidden-mobile">
                <span class="icon">
                  <i class="fas fa-trash-alt"></i>
                </span>
              </a> @endrole
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>



<div class="card m-t-20" v-if="tab == 'waiting'" v-cloak>
  <div class="card-content">
    <table class="table is-hoverable is-fullwidth">
      <thead>
        <tr>
          <th>Nom</th>
          <th class="is-hidden-mobile">Lieu</th>
          <th>Date</th>
          <th class="is-hidden-mobile">Categorie</th>
          <th class="is-hidden-mobile"></th>
          <th class="is-hidden-mobile"></th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        @foreach ($waiting as $w)
        <tr>
          <td>{{$w->nom}}</td>
          <td class="is-hidden-mobile">{{$w->lieu}}</td>
          <td>{{ \Carbon\Carbon::parse($w->date)->format('d/m/Y')}}</td>
          <td class="is-hidden-mobile">{{$w->categorie}}</td>
          <td class="is-hidden-mobile">
            @if($w->is_selection == 1)
            <span class="tag is-danger is-rounded m-l-5">
          <span class="icon m-r-2"><i class="fas fa-exclamation-circle"></i></span> sur sélection
            </span>
            @endif
          </td>

          <td class="is-hidden-mobile">
            @if($w->is_qualificatif == 1)
            <span class="tag is-success is-rounded m-l-5">
          <span class="icon m-r-2"><i class="fas fa-star"></i></span> qualificatif
            </span>@endif
          </td>

          <td class="has-text-right">
            <a href="{{route('calendrier.show', $w->id)}}" class="button is-outlined">
              <span class="icon">
                <i class="far fa-eye"></i>
              </span>
            </a>
            <a href="{{route('calendrier.edit', $w->id)}}" class="is-hidden-mobile button is-outlined is-hidden-mobile">
                <span class="icon">
                  <i class="fas fa-pencil-alt"></i>
                </span>
              </a>@role('superadministrator|administrator|moderator')
            <a href="{{route('calendrier.accept', $w->id)}}" class="is-hidden-mobile button is-outlined m-l-20 is-success">
              <span class="icon">
                  <i class="fas fa-check"></i>
              </span>
            </a>
            <a href="{{route('calendrier.reject', $w->id)}}" class="is-hidden-mobile button is-outlined is-danger">
              <span class="icon">
                  <i class="fas fa-times"></i>
              </span>
            </a> @endrole
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>


<div class="card" v-if="tab == 'archives'" v-cloak>


  <div class="card-content">
    <table class="table is-hoverable is-fullwidth">
      <thead>
        <tr>
          <th>Nom</th>
          <th class="is-hidden-mobile">Lieu</th>
          <th>Date</th>
          <th class="is-hidden-mobile">Categorie</th>
          <th class="is-hidden-mobile"></th>
          <th class="is-hidden-mobile"></th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        @foreach ($archives as $a)
        <tr>
          <td>{{$a->nom}}</td>
          <td class="is-hidden-mobile">{{$a->lieu}}</td>
          <td>{{ \Carbon\Carbon::parse($a->date)->format('d/m/Y')}}</td>
          <td class="is-hidden-mobile">{{$a->categorie}}</td>
          <td class="is-hidden-mobile">
            @if($a->is_selection == 1)
            <span class="tag is-danger is-rounded m-l-5">
                <span class="icon m-r-2"><i class="fas fa-exclamation-circle"></i></span> sur sélection
            </span>
            @endif
          </td>

          <td class="is-hidden-mobile">
            @if($a->is_qualificatif == 1)
            <span class="tag is-success is-rounded m-l-5">
                <span class="icon m-r-2"><i class="fas fa-star"></i></span> qualificatif
            </span>@endif
          </td>

          <td class="has-text-right">
            <a href="{{route('calendrier.show', $a->id)}}" class="button is-outlined">
              <span class="icon">
                <i class="far fa-eye"></i>
              </span>
            </a>
          </td>
        </tr>
        @endforeach
        
      </tbody>
    </table>
  </div>

</div>

{{-- {{$evenements->links('vendor.pagination.default')}} --}}
@endsection
 
@section('scripts')
<script>
  var app = new Vue({
      el: '#app',
      data: {
        tab: 'published'
      }
    });

</script>
@endsection