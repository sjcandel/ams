@extends('layouts.layouts-intranet.master') 
@section('content')

<div class="columns is-mobile">
  <div class="column">
    <h1 class="title">
      <a href="{{route('actualites.index')}}" class="button is-outlined m-r-10">
        <span class="icon">
            <i class="fas fa-chevron-left"></i>
        </span>
      </a> Détails
    </h1>
  </div>
  <div class="column has-text-right">
    @if (Laratrust::can('update-actualites'))
    <a href="{{route('actualites.edit', $actualite->id)}}" class="button is-pulled-right">
        <span class="icon is-small">
            <i class="fas fa-pencil-alt"></i>
        </span>
        <span>Modifier</span>
    </a>@endif
  </div>
</div>
<hr>

<div class="columns">
  <div class="column">
    <div class="card">
      <div class="card-content">

        <div class="field">
          <label for="author" class="label">Auteur</label>
          <p class="pre">{{$actualite->user->prenom}} {{$actualite->user->nom}}</p>
        </div>
        <div class="field">
          <label for="title" class="label">Titre</label>
          <p class="pre">{{$actualite->title}}</p>
        </div>
        <div class="field">
          <label for="content" class="label">Contenu</label>
          <div class="pre">{!! $actualite->content !!}</div>
        </div>

        {{-- @if ($actualite->image)
        <div class="field">
          <label for="content" class="label">Photo</label>
          <figure class="imageContainer">
            <img src="{{asset('uploads/assets/photos/actualites/'.$actualite->image) }}">
          </figure>
        </div>
        @endif --}}

      </div>
    </div>

  </div>
  <div class="column is-one-quarter">
    <div class="card">
      <div class="card-content">
        

        @if ($actualite->image)
        <div class="field">
          <label for="content" class="label">Photo</label>
          <figure class="imageContainer">
            <img src="{{asset('uploads/assets/photos/actualites/'.$actualite->image) }}">
          </figure>
        </div>
        @endif
        {{--
        <hr> @if ($actualite->admin_validation == 0) @role('superadministrator|administrator')
        <a href="{{route('actualites.accept', $actualite->id)}}" class="button is-outlined is-success is-fullwidth m-b-10">
            <span class="icon is-small">
                <i class="fas fa-check"></i> 
            </span>
            <span>Valider</span>
          </a>
        <a href="{{route('actualites.reject', $actualite->id)}}" class="button is-outlined is-danger is-fullwidth">
            <span class="icon is-small">
                <i class="fas fa-times"></i> 
            </span>
            <span>Refuser</span>
          </a> @endrole @endif
        <hr>--}}

      </div>

    </div>
  </div>
</div>
@endsection
 
@section('scripts')
@endsection