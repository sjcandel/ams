@extends('layouts.layouts-intranet.master') 
@section('content')

<div class="columns is-mobile">
  <div class="column">
    <h1 class="title">Actualités</h1>
  </div>
  <div class="column has-text-right is-one-quarter">
    @if (Laratrust::can('create-actualites'))
    <a href="{{ route('actualites.create') }}" class="button is-primary has-text-white is-size-5"><i class="fas fa-plus"></i></a>@endif
  </div>
</div>

<div class="tabs">
  <ul>
    <li v-bind:class="{ 'is-active': tab == 'published' }" v-on:click="tab = 'published'">
      <a>
        <span class="icon is-small"><i class="fas fa-edit"></i></span>
        <span>Publiées <span class="tag is-rounded is-white"><b class="has-text-grey">{{$actualitesCount}}</b></span></span>
      </a>
    </li>
    <li v-bind:class="{ 'is-active': tab == 'waiting' }" v-on:click="tab = 'waiting'">
      <a>
        <span class="icon is-small"><i class="fas fa-hourglass-half"></i></i></span>
        <span>En attente<span class="tag is-rounded is-white"><b class="has-text-grey">{{$waitingCount}}</b></span></span>
      </a>
    </li>
    {{-- <li v-bind:class="{ 'is-active': tab == 'drafts' }" v-on:click="tab = 'drafts'">
      <a>
        <span class="icon is-small"><i class="fas fa-folder-open"></i></span>
        <span>Brouillons <span class="tag is-rounded is-white"><b class="has-text-grey">{{$draftCount}}</b></span></span>
      </a>
    </li> --}}

  </ul>
</div>

<div class="card" v-if="tab == 'published'">
  <div class="card-content">

    <table class="table is-hoverable is-fullwidth">
      <thead>
        <tr>
          <th>Titre</th>
          <th class="is-hidden-mobile">Auteur</th>
          <th class="is-hidden-mobile">Date de publication</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        @foreach ($actualites as $actualite)
        <tr>
          <td>{{$actualite->title}}</td>
          <td class="is-hidden-mobile">{{$actualite->user->prenom}}</td>
          <td class="is-hidden-mobile">{{\Carbon\Carbon::parse($actualite->created_at)->format('d/m/Y')}}</td>

          <td class="has-text-right">
            <a href="{{route('actualites.show', $actualite->id)}}" class="button is-outlined">
              <span class="icon">
                <i class="far fa-eye"></i>
              </span>
            </a> @role('superadministrator|administrator|moderator')
            <a href="{{route('actualites.edit', $actualite->id)}}" class="button is-outlined is-hidden-mobile">
              <span class="icon">
                <i class="fas fa-pencil-alt"></i>
              </span>
            </a>
            <a href="{{route('actualites.destroy', $actualite->id)}}" class="button is-outlined is-hidden-mobile">
                <span class="icon">
                  <i class="fas fa-trash-alt"></i>
                </span>
              </a> @endrole
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>

<div class="card" v-if="tab == 'waiting'" v-cloak>
  <div class="card-content">
    <table class="table is-hoverable is-fullwidth">
      <thead>
        <tr>
          <th>Titre</th>
          @role('superadministrator|administrator')
          <th>Auteur</th>@endrole
          <th>Date de soumission</th>

          <th></th>
        </tr>
      </thead>
      <tbody>
        @foreach ($waiting as $w)
        <tr>
          <td>{{$w->title}}</td>
          @role('superadministrator|administrator|moderator')
          <td>{{$w->user->prenom}}</td>@endrole
          <td>{{ \Carbon\Carbon::parse($actualite->created_at) }}</td>

          <td class="has-text-right">
            <a href="{{route('actualites.show', $w->id)}}" class="button is-outlined">
              <span class="icon">
                <i class="far fa-eye"></i>
              </span>
            </a>

            <a href="{{route('actualites.edit', $w->id)}}" class="button is-outlined is-hidden-mobile">
              <span class="icon">
                <i class="fas fa-pencil-alt"></i>
              </span>
            </a> @role('superadministrator|administrator|moderator')
            <a href="{{route('actualites.accept', $w->id)}}" class="button is-outlined m-l-20 is-success">
              <span class="icon">
                  <i class="fas fa-check"></i>
              </span>
            </a>
            <a href="{{route('actualites.reject', $w->id)}}" class="button is-outlined is-danger">
              <span class="icon">
                  <i class="fas fa-times"></i>
              </span>
            </a> @endrole
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>

{{-- <div class="card" v-if="tab == 'drafts'" v-cloak>
  <div class="card-content">

    <table class="table is-hoverable is-fullwidth">
      <thead>
        <tr>
          <th>Titre</th>
          <th>Dernières modifications</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        @foreach ($draft as $d)
        <tr>
          <td>{{$d->title}}</td>
          <td>{{$d->created_at}}</td>

          <td class="has-text-right">
            <a href="{{route('actualites.show', $d->id)}}" class="button is-outlined">
                  <span class="icon">
                      <i class="far fa-eye"></i>
                    </span>
              </a>

            <a href="{{route('actualites.edit', $d->id)}}" class="button is-outlined is-hidden-mobile">
                <span class="icon">
                  <i class="fas fa-pencil-alt"></i>
                </span>

              </a> @if (Laratrust::can('delete-actualites'))
            <a href="{{route('actualites.destroy', $d->id)}}" class="button is-outlined is-hidden-mobile">
                <span class="icon">
                  <i class="fas fa-trash-alt"></i>
                </span>
              </a> @endif
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div> --}}


{{-- {{$actualites->links('vendor.pagination.default')}} --}}
@endsection
 
@section('scripts')
<script>
  var app = new Vue({
      el: '#app',
      data: {
        tab: 'published'
      }
    });

</script>
@endsection