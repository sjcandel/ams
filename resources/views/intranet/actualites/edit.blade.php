@extends('layouts.layouts-intranet.master') 
@section('content')

<div class="columns">
  <div class="column">
    <h1 class="title">
      <a href="{{route('actualites.show', $actualite->id)}}" class="button is-outlined m-r-10">
        <span class="icon">
            <i class="fas fa-chevron-left"></i>
        </span>
      </a> Modifier
    </h1>
  </div>
  <div class="column has-text-right">
    <a href="{{route('actualites.edit', $actualite->id)}}" class="button is-primary is-pulled-right">
      <span class="icon is-small">
          <i class="fas fa-pencil-alt"></i>
      </span>
      <span>Modifier</span>
    </a>
  </div>
</div>
<hr>@if($errors->any())
<article class="message is-danger">
  <div class="message-header">
    <span>
      <i class="fas fa-exclamation-triangle"></i> Attention
    </span>
  </div>
  <div class="message-body">
    @foreach($errors->all() as $error)
    <p>{{$error}}</p>
    @endforeach
  </div>
</article>
@endif

<form action="{{route('actualites.update', ['id' => $actualite->id])}}" method="POST" enctype="multipart/form-data">
  {{ csrf_field() }} {{ method_field('PUT') }}

  <div class="columns">
    <div class="column">
      <div class="card">
        <div class="card-content">
          <div class="field">
            <label for="title" class="label">Titre</label>
            <p class="control">
              <input type="text" class="input" name="title" id="title" value="{{$actualite->title}}">
            </p>
          </div>
          <div class="field">
            <label for="content" class="label">Contenu</label>
            <p class="control">
              <textarea class="textarea" name="content" id="content" rows="10">{{$actualite->content}}</textarea>
            </p>
          </div>

          

          {{--
          <div class="field">
            <label for="content" class="label">Photo</label>
            <figure class="imageContainer">
              <img src="{{asset('uploads/assets/photos/actualites/'.$actualite->image) }}">
            </figure>
            <div class="file is-centered m-t-20 is-fullwidth">
              <label class="file-label" for="file">
                      <input class="file-input" type="file" name="file" id="file">
                      <span class="file-cta">
                          <span class="file-icon">
                              <i class="fas fa-upload"></i>
                          </span>
                          <span class="file-label">
                              Modifier la photo...
                          </span>
                      </span>
                  </label>
            </div>
          </div> --}}
        </div>
      </div>

    </div>
    <div class="column is-one-quarter">
      <div class="card">
        <div class="card-content">
            @if ($actualite->image)
            <div class="field">
              <label for="content" class="label">Photo</label>
              <figure v-if="!image" class="imageContainer">
                <img src="{{asset('uploads/assets/photos/actualites/'.$actualite->image) }}">
              </figure>
            </div>
            @endif
  
            <figure v-if="image" class="imageContainer">
              <img :src="image">
            </figure>
            <div class="file is-centered m-t-20 is-fullwidth">
              <label class="file-label" for="file">
                  <input class="file-input" type="file" name="file" id="file" @change="onFileChange">
                  <span class="file-cta">
                      <span class="file-icon">
                          <i class="fas fa-upload"></i>
                      </span>
                      <span class="file-label">
                          Modifier la photo...
                      </span>
                  </span>
              </label>
            </div>
        </div>

      </div>
    </div>
  </div>


  <div class="columns">
    <div class="column">
      <div class="field is-grouped">
        <p class="control">
          <a href="{{route('actualites.index')}}" class="button is-danger  is-fullwidth">
            <span class="icon is-small">
              <i class="fas fa-ban"></i>
            </span>
            <span>Annuler</span>
          </a>
        </p>
        <p class="control">
          <button class="button is-success is-fullwidth">
            <span class="icon is-small">
              <i class="fas fa-check"></i>
            </span>
            <span>Enregistrer</span>
          </button>
        </p>
      </div>
    </div>
    <div class="column is-one-quarter"></div>
  </div>


</form>
@endsection
 
@section('scripts')
<script>
  var app = new Vue({
    el: '#app',
    data: {
      image: ''
    },
    methods: {
      onFileChange(e) {
        var files = e.target.files || e.dataTransfer.files;
        if (!files.length)
            return;
        this.createImage(files[0]);
        },
      createImage(file) {
        var image = new Image();
        var reader = new FileReader();
        var vm = this;

        reader.onload = (e) => {
            vm.image = e.target.result;
        };
        reader.readAsDataURL(file);
      }
    }
  });

</script>


<script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
<script>
  CKEDITOR.replace('content', {
    toolbarGroups: [
        {"name":"basicstyles","groups":["basicstyles"]},
        {"name":"links","groups":["links"]},
        {"name":"paragraph","groups":["list","blocks"]},
        {"name":"document","groups":["mode"]},
        {"name":"insert","groups":["insert"]},
        {"name":"styles","groups":["styles"]}
    ],
    removeButtons: 'Underline,Strike,Subscript,Superscript,Anchor,Styles,Specialchar,Image',
});

</script>
@endsection