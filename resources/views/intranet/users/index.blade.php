@extends('layouts.layouts-intranet.master') 
@section('content')

<div class="columns is-mobile">
  <div class="column">
    <h1 class="title">Utilisateurs</h1>
  </div>
  @if (Laratrust::can('create-utilisateurs'))
  <div class="column has-text-right is-one-quarter">
    <a href="{{ route('users.create') }}" class="button is-primary has-text-white is-size-5"><i class="fas fa-plus"></i></a>
  </div>@endif
</div>
<hr>

<div class="columns">
  @foreach ($users as $user)
  <div class="column is-one-quarter">
    <div class="box">
      <div class="media">
        <div class="media-content has-text-centered">
          <div class="avatar-round-big"><img src="{{asset('uploads/assets/photos/users/'.$user->photo) }}"></div>
          <p class="is-size-5 m-t-15"><b>{{$user->prenom}} {{$user->nom}}</b></p>
          <p>{{$user->email}}</p>
          <hr>
          <div>
            <a href="{{route('users.show', $user->id)}}" class="button is-outlined">
              <span class="icon">
                <i class="far fa-eye"></i>
              </span>
            </a> @if (Laratrust::can('update-utilisateurs'))
            <a href="{{route('users.edit', $user->id)}}" class="button is-outlined">
              <span class="icon">
                <i class="fas fa-pencil-alt"></i>
              </span>
            </a>@endif @if (Laratrust::can('delete-utilisateurs'))
            <a href="{{route('users.destroy', $user->id)}}" class="button is-outlined">
              <span class="icon">
                <i class="fas fa-trash-alt"></i>
              </span>
            </a>@endif
          </div>
        </div>
      </div>
    </div>
  </div>@endforeach
</div>


{{$users->links('vendor.pagination.default')}}
@endsection