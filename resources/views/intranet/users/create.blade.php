@extends('layouts.layouts-intranet.master') 
@section('content')

<div class="columns">
    <div class="column">
        <h1 class="title">
            <a href="{{route('users.index')}}" class="button is-outlined m-r-10">
                <span class="icon">
                    <i class="fas fa-chevron-left"></i>
                </span>
            </a>Nouvel utilisateur
        </h1>
    </div>
</div>
<hr> @if($errors->any())
<article class="message is-danger">
    <div class="message-header">
        <span><i class="fas fa-exclamation-triangle"></i> Attention</span>
    </div>
    <div class="message-body">
        @foreach($errors->all() as $error)
        <p>{{$error}}</p>
        @endforeach
    </div>
</article>
@endif

<form action="{{route('users.store')}}" method="POST" enctype="multipart/form-data">
    {{ csrf_field() }}

    <div class="columns">
        <div class="column">
            <div class="card">
                <div class="card-content">
                    <div class="field">
                        <label for="prenom" class="label">Prenom <span><sup class="has-text-danger">*</sup></span></label>
                        <p class="control">
                            <input type="text" class="input" name="prenom" id="prenom">
                        </p>
                    </div>
                    <div class="field">
                        <label for="nom" class="label">Nom <span><sup class="has-text-danger">*</sup></span></label>
                        <p class="control">
                            <input type="text" class="input" name="nom" id="nom">
                        </p>
                    </div>
                    <div class="field">
                        <label for="email" class="label">Email <span><sup class="has-text-danger">*</sup></span></label>
                        <p class="control">
                            <input type="text" class="input" name="email" id="email">
                        </p>
                    </div>

                    <div class="field">
                        <label for="password" class="label">Mot de passe <span><sup class="has-text-danger">*</sup></span></label>
                        <p class="control">
                            <input type="text" class="input" name="password" id="password" v-if="!auto_password">
                            <b-checkbox name="auto_generate" class="m-t-15" v-model="auto_password">Générer un mot de passe aléatoire</b-checkbox>
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <div class="column is-one-quarter">
            <div class="card">
                <div class="card-content">
                    <div class="file is-boxed" v-if="!image">
                        <label class="file-label" for="file">
                            <input class="file-input" type="file" name="file" id="file" @change="onFileChange">
                            <span class="file-cta">
                                <span class="file-icon">
                                  <i class="fas fa-upload"></i>
                                </span>
                                <span class="file-label">
                                    Importer une photo...
                                </span>
                            </span>
                        </label>
                    </div>
                    <div v-else v-cloak>
                        <figure class="image is-2by3 avatar">
                            <img :src="image">
                        </figure>
                        <div class="file is-centered m-t-20 is-fullwidth">
                            <label class="file-label" for="file">
                                <input class="file-input" type="file" name="file" id="file" @change="onFileChange">
                                <span class="file-cta">
                                    <span class="file-icon">
                                        <i class="fas fa-upload"></i>
                                    </span>
                                    <span class="file-label">
                                        Modifier la photo...
                                    </span>
                                </span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="columns">
        <div class="column">
            <div class="field is-grouped m-t-15">
                <p class="control">
                    <a href="{{route('users.index')}}" class="button is-danger  is-fullwidth">
                    
                    <span class="icon is-small">
                        <i class="fas fa-ban"></i>
                    </span>
                    <span>Annuler</span>
                    </a>
                </p>
                <p class="control">
                    <button class="button is-success is-fullwidth">
                        
                        <span class="icon is-small">
                            <i class="fas fa-check"></i>
                        </span>
                        <span>Enregistrer</span>
                    </button>
                </p>
            </div>
        </div>
        <div class="column is-one-quarter"></div>
    </div>
</form>
{{-- </div> --}}
@endsection
 
@section('scripts')
<script>
    var app = new Vue({
        el: '#app',
        data: {
            auto_password: false,
            image: ''
        },
        methods: {
            onFileChange(e) {
            var files = e.target.files || e.dataTransfer.files;
            if (!files.length)
                return;
            this.createImage(files[0]);
            },
            createImage(file) {
            var image = new Image();
            var reader = new FileReader();
            var vm = this;

            reader.onload = (e) => {
                vm.image = e.target.result;
            };
            reader.readAsDataURL(file);
            }
        }
})

</script>
@endsection