@extends('layouts.layouts-intranet.master') 
@section('content')

<div class="columns">
    <div class="column">
        <h1 class="title">
            <a href="{{route('intranet')}}" class="button is-outlined m-r-10">
                <span class="icon">
                    <i class="fas fa-chevron-left"></i>
                </span>
            </a>Profil
        </h1>
    </div>
</div>

<hr>

<div class="columns">
    <div class="column">
        <div class="card">
            <div class="card-content">
                <div class="columns">
                    <div class="column field">
                        <label for="prenom" class="label">Prénom</label>
                        <p class="pre">{{Auth::user()->prenom}}</p>
                    </div>
                    <div class="column field">
                        <label for="nom" class="label">Nom</label>
                        <p class="pre">{{Auth::user()->nom}}</p>
                    </div>
                </div>
                <div class="field">
                    <label for="email" class="label">Email</label>
                    <p class="pre">{{Auth::user()->email}}</p>
                </div>
                
                <hr>
                <div>
                    @if (Laratrust::can('update-profile'))
                    <a href="{{route('editProfile')}}" class="button is-primary">
                        <span class="icon is-small">
                            <i class="fas fa-lock"></i>
                        </span>
                        <span>Modifier le mot de passe</span>
                    </a>@endif
                </div>
            </div>
        </div>
    </div>

    <div class="column is-one-quarter">
        <div class="card">
            <div class="card-content">
                <figure class="image is-2by3 avatar">
                    <img src="/uploads/assets/photos/users/{{ Auth::user()->photo }}">
                </figure>
            </div>
        </div>
    </div>
</div>
@endsection