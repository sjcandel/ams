@extends('layouts.layouts-intranet.master') 
@section('content')

<div class="columns is-mobile">
  <div class="column">
    <h1 class="title">
        <a href="{{route('showProfile')}}" class="button is-outlined m-r-10">
            <span class="icon">
                <i class="fas fa-chevron-left"></i>
            </span>
        </a>Modifier le mot de passe
    </h1>
  </div>
</div>
<hr>



<form method="POST" action="{{ route('updateProfile') }}">
        {{ csrf_field() }}

    @if (session('error'))
        <article class="message is-danger">
            <div class="message-header">
                <span><i class="fas fa-exclamation-triangle"></i> Attention</span>
            </div>
            <div class="message-body">
                {{ session('error') }}
            </div>
        </article>
    @endif
    @if (session('success'))
        <article class="message is-success">
            <div class="message-header">
                <span><i class="fas fa-check-circle"></i> Succes</span>
            </div>
            <div class="message-body">
                {{ session('success') }}
            </div>
        </article>
    @endif

    <div class="card">
        <div class="card-content">
          
            <div class="form-group{{ $errors->has('current-password') ? ' has-error' : '' }} field">
                <label for="new-password" class="label">Mot de passe actuel <span><sup class="has-text-danger">*</sup></span></label>
                <p class="control">
                    <input id="current-password" type="password" class="input" name="current-password">
                </p>
            </div>
            <div class="form-group{{ $errors->has('new-password') ? ' has-error' : '' }} field">
                <label for="new-password" class="label">Nouveau mot de passe <span><sup class="has-text-danger">*</sup></span></label>
                <p class="control">
                    <input id="new-password" type="password" class="input" name="new-password">
                </p>
            </div>
            <div class="field">
                <label for="new-password-confirm" class="label">Confirmation <span><sup class="has-text-danger">*</sup></span></label>
                <p class="control">
                    <input id="new-password-confirm" type="password" class="input" name="new-password_confirmation">
                </p>
            </div>

        </div>
    </div>

    
    <div class="field is-grouped m-t-20">
        <p class="control">
            <a href="{{route('showProfile')}}" class="button is-danger  is-fullwidth">
            <span class="icon is-small">
                <i class="fas fa-ban"></i>
            </span>
            <span>Annuler</span>
            </a>
        </p>
        <p class="control">
            <button class="button is-success is-fullwidth">
                <span class="icon is-small">
                    <i class="fas fa-check"></i>
                </span>
                <span>Enregistrer</span>
            </button>
        </p>
    </div>

</form>
@endsection
 
@section('scripts')
@endsection




{{-- 
<div class="columns">
    <div class="column is-one-third is-offset-one-third m-t-100 m-b-100">
        <div class="card">
            <div class="card-content">
                <p class="title">Modifier le mot de passe</p>
                    

                


            </div>
        </div>
    </div>
</div> --}}

