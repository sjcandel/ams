@extends('layouts.master') 
@section('body')

<section class="hero is-primary is-fullheight has-text-centered">
  <div class="hero-body">
    <div class="container">

      <p class="mainTitle animated tada">Oups ...</p>
      <p class="is-size-3 has-text-weight-light m-b-60 animated tada">Vous n'êtes pas autorisé à voir cette page !</p>
      <a class="button is-primary is-inverted is-outlined animated fadeInUp delay-1s" href="{{route('home')}}">
        <span class="icon is-small">
            <i class="fas fa-arrow-left"></i>
        </span> 
        <span>Retourner à l'accueil</span>
      </a>

    </div>
  </div>
</section>
@endsection