@extends('layouts.layouts-app.master') 
@section('pageTitle')Arts Martiaux Schweighouse - judo loisir et compétition
@endsection


@section('pageDescription')Bienvenue sur le site officiel du club des Arts Martiaux de Schweighouse/Moder. Nous proposons
des cours de judo loisir et compétition adaptés aux enfants à partir de 4 ans ainsi que des ateliers de remise en forme pour
les adultes.
@endsection
 
@section('content')

<div class="has-background-primary">
    <section class="section parallax px-is-home animated fadeIn slow">
        <div class="container">
            <div class="animated bounceInUp">
                <h1 class="mainTitle">Arts Martiaux Schweighouse</h1>
                <a href="https://www.facebook.com/AMSchweighouse/" target="_blank" class="m-b-20"><i class="fab fa-facebook"></i> AMSchweighouse</a>
            </div>
        </div>
    </section>
</div>

<section class="section">
    <div class="container">
        <div class="columns is-horizontal-evenly">
            <div class="column is-one-third is-half-tablet has-text-centered">
                <h2 class="is-size-5"><b>Cours de judo</b></h2>
                <p class="is-size-7 is-italic m-b-10">loisir et compétition</p>
                <p>Le Judo est un art martial qui convient aussi bien aux enfants qu’aux adultes. </p>
                <p>Grâce aux valeurs qu'il véhicule, chacun se fera plaisir quel que soit son niveau et ses objectifs. </p>
                <a class="button is-fullwidth m-t-10" href="{{route('horaires-tarifs.horaires')}}">Horaires...</a>
            </div>
            <div class="column is-one-third is-half-tablet has-text-centered">
                <h2 class="is-size-5"><b>Ateliers adultes</b></h2>
                <p class="is-size-7 is-italic m-b-10">remise en forme et détente</p>
                <p>Cette activité permet d'entretenir sa forme physique de façon ludique, douce et conviviale. Les exercices
                    proposés développent aussi bien la coordination que l'équilibre et la souplesse.</p>
                <a class="button is-fullwidth m-t-10" href="{{route('horaires-tarifs.horaires')}}">Horaires...</a>
            </div>
        </div>
    </div>
</section>

<section class="section has-background-primary">
    <div class="container">
        <p class="subTitle has-text-centered has-text-white m-b-30">Bruits du dojo</p>
        <div class="masonryLayout">
            @foreach ($actualites as $actualite)
            <div class="masonryLayout_item">
                <div class="message is-white">
                    <div class="message-body">
                        <p class="is-size-5"><b>{{$actualite->title}}</b></p>
                        <p class="is-size-7 is-italic m-b-10">le {{\Carbon\Carbon::parse($actualite->created_at)->format('d/m/Y')}} par {{$actualite->user->prenom}}</p>
                        <hr>
                        @if($actualite->image)
                        <figure>
                            <img src="{{asset('uploads/assets/photos/actualites/'.$actualite->image) }}" alt="{{$actualite->title}}">
                        </figure>@endif
                        <p>{!! $actualite->content !!}</p>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>

<section class="section" id="contact">
    <div class="container">

        <div class="columns is-horizontal-extremes">
            <div class="column m-r-30">
                <p class="subTitle m-b-30">Restons en contact !</p>
                <div class="box">
                    <div class="media">
                        <div class="media-content">
                            <p class="m-b-20"><i class="fab fa-facebook"></i> <b>Facebook - </b><a href="https://www.facebook.com/AMSchweighouse/" target="_blank">AMSchweighouse</a></p>
                            <p><i class="fas fa-phone"></i> 06 89 42 89 91</p>

                            <hr class="m-t-20 m-b-20">

                            <p><i class="fas fa-map-marker-alt"></i> <b>Dojo de Schweighouse</b></p>
                            <p>Centre Robert Kaeuffling</p>
                            <p class="m-b-20">10 rue des sports - 67590 Schweighouse sur Moder</p>

                            <p><i class="fas fa-map-marker-alt"></i> <b>Dojo de Morsbronn</b></p>
                            <p>Salle polyvalente</p>
                            <p>Route de Haguenau - 67360 Morsbronn-lès-Bains</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="column m-l-30">

                <form action="{{route('users.store')}}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="columns m-b-0">
                        <div class="column">
                            <label for="email" class="label">Email</label>
                            <p class="control">
                                <input type="text" class="input" name="email" id="email">
                            </p>
                        </div>
                        <div class="column">
                            <label for="objet" class="label">Sujet</label>
                            <p class="control">
                                <input type="text" class="input" name="objet" id="objet">
                            </p>
                        </div>
                    </div>

                    <label for="content" class="label m-t-10">Message</label>
                    <p class="control">
                        <textarea class="textarea" name="content" id="content" rows="8"></textarea>
                    </p>

                    <div class="field is-grouped m-t-20">
                        <p class="control">
                            <button class="button is-fullwidth is-primary is-outlined">
                                <span class="icon is-small">
                                    <i class="fas fa-paper-plane"></i>
                                </span>
                                <span>Envoyer</span>
                            </button>
                        </p>
                    </div>
                </form>

            </div>
        </div>

    </div>
</section>
@endsection
 
@section('scripts')
@endsection