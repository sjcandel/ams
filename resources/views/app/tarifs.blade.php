@extends('layouts.layouts-app.master') 
@section('pageTitle') Tarifs - Arts Martiaux
Schweighouse
@endsection
 
@section('content')
<section class="section parallax px-is-tarifs">
    <div class="container">
        <h1 class="mainTitle">Tarifs</h1>
    </div>
</section>

<section class="container has-background-white">
    <article class="mainContent">

        <div class="columns is-horizontal-evenly">
            <div class="column is-two-fifths">
                <div>
                    <p class="has-text-weight-bold m-b-20">Tarif annuel pour 1 judoka</p>

                    <ul class="leaders">
                        <li>
                            <span>Licence FFJDA</span>
                            <span>37€</span>
                        </li>
                        <li>
                            <span>Cotisation club</span>
                            <span>73€</span>
                        </li>
                        <li class="is-uppercase has-text-weight-bold m-t-20">
                            <span>Total</span>
                            <span>110€</span>
                        </li>
                    </ul>
                    
                </div>
                <hr>
                <p class="m-b-20">Les familles qui ont ont plusieurs licenciés au club bénéficieront d'une remise à partir du deuxième judoka : </p>
                

                <ul class="leaders">
                    <li>
                        <span>2 judokas</span>
                        <span>210€</span>
                    </li>
                    <li>
                        <span>3 judokas</span>
                        <span>315€</span>
                    </li>
                    <li>
                        <span>4 judokas</span>
                        <span>420€</span>
                    </li>
                </ul>
            </div>

        
                
            <div class="column is-two-fifths has-background-white-bis border">
                <p class="has-text-weight-bold">Documents à télécharger</p>
                <hr>
                <div class="m-t-20 m-b-20 columns is-horizontal-evenly has-text-centered is-mobile">
                    <div>
                        <a href="uploads/assets/docs/AMS-licence.pdf" download>
                            <i class="fas fa-file-pdf is-size-2"></i>
                            <p>Licence</p>
                        </a>
                    </div>
                    <div>
                        <a href="uploads/assets/docs/AMS-autorisation parentale.pdf" download>
                            <i class="fas fa-file-pdf is-size-2"></i>
                            <p>Autorisation parentale</p>
                        </a>
                    </div>
                    <div>
                        <a href="uploads/assets/docs/AMS-certificat medical.pdf" download>
                            <i class="fas fa-file-pdf is-size-2"></i>
                            <p>Certificat médical</p>
                        </a>
                    </div>
                </div>
            </div>
            
        </div>

    </article>
</section>
@endsection