@extends('layouts.layouts-app.master')
@section('pageTitle') Galerie - Arts Martiaux
Schweighouse
@endsection
 
@section('content')
<section class="section parallax px-is-galerie">
    <div class="container">
        <h1 class="mainTitle">Galerie</h1>
    </div>
</section>

<section class="container has-background-white">
    <article class="mainContent">
        <div class="columns is-mobile">
            <div class="column is-1">
                <a href="{{route('galerie')}}" class="button is-outlined m-r-10">
                    <span class="icon">
                        <i class="fas fa-chevron-left"></i>
                    </span>
                </a> 
            </div>
        
            <div class="column">
                <p class="is-size-5"><b>{{$album->titre}}</b></p>
                <p class="m-b-20 is-italic">{{$album->lieu}} le {{\Carbon\Carbon::parse($album->date)->format('d/m/Y')}}</p>
            </div>
        </div>

        <hr class="m-t-0 m-b-40">

        <div class="columns">
            @foreach($photos as $photo)
                <div class="column is-one-third">
                    <figure class="image is-5by4 avatar">
                        <img src="{{asset('uploads/assets/photos/galerie/'.$photo->nom)}}">
                    </figure>
                </div>
            @endforeach
        </div>
   
    </article>
</section>
@endsection