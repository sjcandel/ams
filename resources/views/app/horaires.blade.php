@extends('layouts.layouts-app.master')
@section('pageTitle') Horaires - Arts Martiaux
Schweighouse
@endsection
 
@section('content')
<section class="section parallax px-is-horaires">
    <div class="container">
        <h1 class="mainTitle">Horaires</h1>
    </div>
</section>

<section class="container has-background-white">
    <article class="mainContent">
        <div class="tabs m-b-50">
            <ul>
                <li v-bind:class="{ 'is-active': tab == 'judo' }" v-on:click="tab = 'judo'">
                    <a>
                        <span class="icon is-small"><i class="fas fa-chevron-right"></i></span>
                        <span>Cours de Judo</span>
                    </a>
                </li>
                <li v-bind:class="{ 'is-active': tab == 'taiso' }" v-on:click="tab = 'taiso'">
                    <a>
                        <span class="icon is-small"><i class="fas fa-chevron-right"></i></span>
                        <span>Ateliers adultes</span>
                    </a>
                </li>
            </ul>
        </div>

        <div v-if="tab == 'judo'">
            
            {{-- <div class="has-text-danger has-text-weight-bold m-b-20">
                <i class="fas fa-exclamation-circle"></i>
                Changement d'horaires à partir du 1er janvier 2019
            </div> --}}
            <table class="table is-bordered is-striped is-fullwidth">
                <thead>
                    <tr>
                        <th></th>
                        <th>Lundi</th>
                        <th>Mardi</th>
                        <th>Jeudi</th>
                        <th>Vendredi</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th>Baby Judo
                            <p>2014 / 2013</p>
                        </th>
                        <td>
                            <p>16h45 - 17h30</p>
                            <p class="is-size-7 is-italic">Schweighouse</p>
                        </td>
                        <td>
                            <p>17h - 18h</p>
                            <p class="is-size-7 is-italic">Morsbronn</p>
                        </td>
                        <td>
                            <p>16h45 - 17h30</p>
                            <p class="is-size-7 is-italic">Schweighouse</p>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <th>Mini poussins
                            <p>2012 / 2011</p>
                        </th>
                        <td>
                            <p>17h30 - 18h30</p>
                            <p class="is-size-7 is-italic">Schweighouse</p>
                        </td>
                        <td rowspan="2">
                            <p>18h - 19h15</p>
                            <p class="is-size-7 is-italic">Morsbronn</p>
                        </td>
                        <td>
                            <p>17h30 - 18h30</p>
                            <p class="is-size-7 is-italic">Schweighouse</p>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <th>Poussins
                            <p>2010 / 2009</p>
                        </th>
                        <td>
                            <p>18h30 - 19h30</p>
                            <p class="is-size-7 is-italic">Schweighouse</p>
                        </td>
                        <td>
                            <p>18h30 - 19h30</p>
                            <p class="is-size-7 is-italic">Schweighouse</p>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <th>Benjamins
                            <p>2008 / 2007</p>
                        </th>
                        <td  rowspan="2">
                            <p>19h30 - 20h30</p>
                            <p class="is-size-7 is-italic">Schweighouse</p>
                        </td>
                        <td rowspan="2">
                            <p>19h15 - 20h30</p>
                            <p class="is-size-7 is-italic">Morsbronn</p>
                        </td>
                        <td rowspan="2">
                            <p>19h30 - 20h30</p>
                            <p class="is-size-7 is-italic">Schweighouse</p>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <th>Minimes
                            <p>2006 / 2005</p>
                        </th>
                        {{-- <td>
                            <p>19h15 - 20h30</p>
                            <p class="is-size-7 is-italic">Schweighouse</p>
                        </td>
                        <td>
                            <p>19h15 - 20h30</p>
                            <p class="is-size-7 is-italic">Schweighouse</p>
                        </td> --}}
                        <td rowspan="2">
                            <p>19h30 - 21h</p>
                            <p class="is-size-7 is-italic">Schweighouse</p>
                        </td>
                    </tr>
                    <tr>
                        <th>Cadets / Juniors / Seniors
                            <p>2004 et avant</p>
                        </th>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div v-if="tab == 'taiso'" v-cloak>
            {{-- <p class="m-b-40 has-text-centered">Les ateliers ont lieu les mardi de 19h30 à 21h au dojo de Schweighouse (10 rue des sports) selon le planning
                suivant :</p>
            <div class="columns has-text-centered">
                <div class="column">
                    <figure class="m-b-20">
                        <img src="" alt="rentrée zen">
                    </figure>
                    <p class="is-size-5"><b>Rentrée Zen</b></p>
                    <p class="is-size-7 is-italic m-b-10">21.09.18 / 28.09.18 / 05.10.18</p>
                </div>
                <div class="column">
                    <figure class="m-b-20">
                        <img src="" alt="rentrée zen">
                    </figure>
                    <p class="is-size-5"><b>Rentrée Zen</b></p>
                    <p class="is-size-7 is-italic m-b-10">21.09.18 / 28.09.18 / 05.10.18</p>
                </div>
                <div class="column">
                    <figure class="m-b-20">
                        <img src="" alt="rentrée zen">
                    </figure>
                    <p class="is-size-5"><b>Summer Camp</b></p>
                    <p class="is-size-7 is-italic m-b-10">21.09.18 / 28.09.18 / 05.10.18</p>
                </div>
            </div> --}}
            <p>[page en construction ...]</p>
        </div>
    </article>
</section>
@endsection
 
@section('scripts')
<script>
    var app = new Vue({
      el: '#app',
      data: {
        tab: 'judo'
      }
    });

</script>
@endsection