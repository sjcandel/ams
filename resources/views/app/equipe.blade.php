@extends('layouts.layouts-app.master')
@section('pageTitle') Equipe - Arts Martiaux
Schweighouse
@endsection
 
@section('content')
<section class="section parallax px-is-equipe">
    <div class="container">
        <h1 class="mainTitle">Equipe</h1>
    </div>
</section>

<section class="container has-background-white">
    <article class="mainContent">
        <div class="columns is-horizontal-center is-vertical-end">
            <div class="column is-one-third has-text-centered">
                <img src="/uploads/assets/photos/equipe/alain.png" width="250" alt="alain bock">
                <p class="is-size-5 m-0"><b>Alain Bock</b></p>
                <p class="is-size-7 is-italic m-b-10">président</p>
                <p class="m-0"><a href="mailto:bock.alain@orange.fr">bock.alain@orange.fr</a></p>
                <p class="m-0">06 89 42 89 91</p>
            </div>
            <div class="column is-one-third has-text-centered">
                <img src="/uploads/assets/photos/equipe/jacques.png" width="250" alt="jacques candel">
                <p class="is-size-5 m-0"><b>Jacques Candel</b></p>
                <p class="is-size-7 is-italic m-b-10">vice-président</p>
                <p class="m-0"><a href="mailto:jacques.candel@free.fr">jacques.candel@free.fr</a></p>
                <p class="m-0">06 67 33 24 97</p>
            </div>
            <div class="column is-one-third has-text-centered">
                <img src="/uploads/assets/photos/equipe/sylvette.png" width="250" alt="sylvette schmitt">
                <p class="is-size-5 m-0"><b>Sylvette Schmitt</b></p>
                <p class="is-size-7 is-italic m-b-10">vice-présidente</p>
                <p class="m-0"><a href="mailto:schmittdidier67@orange.fr">schmittdidier67@orange.fr</a></p>
                <p class="m-0">03 88 09 58 80</p>
            </div>
            <div class="column is-one-third has-text-centered">
                <img width="250">
                <p class="is-size-5 m-0"><b>Claude Fritsch</b></p>
                <p class="is-size-7 is-italic m-b-10">secrétaire</p>
                <p class="m-0"><a href="mailto:c1893frits@estvideo.fr">c1893frits@estvideo.fr</a></p>
                <p class="m-0">06 83 09 03 91</p>
            </div>
            <div class="column is-one-third has-text-centered">
                <img src="/uploads/assets/photos/equipe/france.png" width="250" alt="france salles">
                <p class="is-size-5 m-0"><b>France Salles</b></p>
                <p class="is-size-7 is-italic m-b-10">comptable</p>
                <p class="m-0"><a href="mailto:francemarine@yahoo.fr">francemarine@yahoo.fr</a></p>
                <p class="m-0">06 16 90 05 28</p>
            </div>
        </div>
    </article>

    <article class="content">
        <div class="columns is-horizontal-center is-vertical-end">
            <div class="column is-one-third has-text-centered">
                <img width="250">
                <p class="is-size-5 m-0"><b>Jérôme Schmitt</b></p>
                <p class="is-size-7 is-italic m-b-10">professeur</p>
                <p class="m-0"><a href="mailto:jerome.schm@yahoo.fr">jerome.schm@yahoo.fr</a></p>
                <p class="m-0">06 85 85 05 96</p>
            </div>
            <div class="column is-one-third has-text-centered">
                <img width="250">
                <p class="is-size-5 m-0"><b>Sacha Difuria</b></p>
                <p class="is-size-7 is-italic m-b-10">professeur</p>
                <p class="m-0"><a href="mailto:sachadifuria@hotmail.fr">sachadifuria@hotmail.fr</a></p>
                <p class="m-0">06 85 24 26 40</p>
            </div>
            <div class="column is-one-third has-text-centered">
                <img src="/uploads/assets/photos/equipe/sophie.png" width="250" alt="sophie candel">
                <p class="is-size-5 m-0"><b>Sophie Candel</b></p>
                <p class="is-size-7 is-italic m-b-10">professeur</p>
                <p class="m-0"><a href="mailto:sophiee.candel@free.fr">sophiee.candel@free.fr</a></p>
                <p class="m-0">06 95 54 88 96</p>
            </div>
        </div>
    </article>
</section>
@endsection