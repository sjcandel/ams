@extends('layouts.layouts-app.master')
@section('pageTitle') Galerie - Arts Martiaux
Schweighouse
@endsection
 
@section('content')
<section class="section parallax px-is-galerie">
    <div class="container">
        <h1 class="mainTitle">Galerie</h1>
    </div>
</section>

<section class="container has-background-white">
    <article class="mainContent">

        <div class="columns">
            @foreach ($albums as $album)
                <div class="column is-one-quarter">
                    <div class="has-background-white-bis border p-t-10 p-b-10 p-l-10 p-r-10">
                        <p class="is-size-5"><b>{{$album->titre}}</b></p>
                        <p class="m-b-20 is-italic">{{$album->lieu}} le {{\Carbon\Carbon::parse($album->date)->format('d/m/Y')}}</p>
                        <hr>
                        <p>
                            <a href="{{route('galerieDetails', $album->id)}}">
                                <i class="far fa-eye"></i>
                                <span>Voir l'album</span>
                            </a>
                        </p>
                    </div>
                </div>
            @endforeach
        </div>

    </article>
</section>
@endsection