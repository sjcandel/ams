@extends('layouts.layouts-app.master') 
@section('pageTitle') Calendriers - Arts Martiaux Schweighouse
@endsection
 
@section('content')

<section class="section parallax px-is-calendrier">
    <div class="container">
        <h1 class="mainTitle">Calendrier</h1>
    </div>
</section>

<section class="container has-background-white">
    <article class="mainContent">
        <div class="m-b-50">
            <p class="is-size-5"><b>Mini-poussins</b> (2012/2011)</p>
            <div class="columns m-t-5">
                @foreach ($minipoussins as $mp)
                    <div class="column is-one-quarter">
                        <div class="has-background-white-bis border p-t-10 p-b-10 p-l-10 p-r-10">
                            <p class="has-text-weight-bold">{{$mp->nom}}</p>
                            <hr class="m-t-10 m-b-10">
                            <p>{{\Carbon\Carbon::parse($mp->date)->format('d/m/Y')}}</p>
                            <p class="is-size-7 is-italic m-t-10">Niveau : {{$mp->niveau}}</p>
                            <p class="is-size-7 is-italic m-b-10">Lieu : {{$mp->lieu}}</p>
                            @if($mp->is_qualificatif == 1)
                            <span class="tag is-success is-rounded m-r-5">
                                <span class="icon m-r-2"><i class="fas fa-star"></i></span>                                    qualificatif
                            </span>
                            @endif
                            @if($mp->is_selection == 1)
                            <span class="tag is-danger is-rounded">
                                <span class="icon m-r-2"><i class="fas fa-exclamation-circle"></i></span>                                    sur sélection
                            </span>
                            @endif
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="m-b-50 m-t-50">
            <p class="is-size-5"><b>Poussins</b> (2010/2009)</p>
            <div class="columns m-t-5">
                @foreach ($poussins as $p)
                    <div class="column is-one-quarter">
                        <div class="has-background-white-bis border p-t-10 p-b-10 p-l-10 p-r-10">
                            <p class="has-text-weight-bold">{{$p->nom}}</p>
                            <hr class="m-t-10 m-b-10">
                            <p>{{\Carbon\Carbon::parse($p->date)->format('d/m/Y')}}</p>
                            <p class="is-size-7 is-italic m-t-10">Niveau : {{$p->niveau}}</p>
                            <p class="is-size-7 is-italic m-b-10">Lieu : {{$p->lieu}}</p>
                            @if($p->is_qualificatif == 1)
                            <span class="tag is-success is-rounded m-r-5">
                                <span class="icon m-r-2"><i class="fas fa-star"></i></span>                                    qualificatif
                            </span>
                            @endif
                            @if($p->is_selection == 1)
                            <span class="tag is-danger is-rounded">
                                <span class="icon m-r-2"><i class="fas fa-exclamation-circle"></i></span>                                    sur sélection
                            </span>
                            @endif
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="m-b-50 m-t-50">
            <p class="is-size-5"><b>Benjamins</b> (2008/2007)</p>
            <div class="columns m-t-5">
                @foreach ($benjamins as $b)
                    <div class="column is-one-quarter">
                        <div class="has-background-white-bis border p-t-10 p-b-10 p-l-10 p-r-10">
                            <p class="has-text-weight-bold">{{$b->nom}}</p>
                            <hr class="m-t-10 m-b-10">
                            <p>{{\Carbon\Carbon::parse($b->date)->format('d/m/Y')}}</p>
                            <p class="is-size-7 is-italic m-t-10">Niveau : {{$b->niveau}}</p>
                            <p class="is-size-7 is-italic m-b-10">Lieu : {{$b->lieu}}</p>
                            @if($b->is_qualificatif == 1)
                            <span class="tag is-success is-rounded m-r-5">
                                <span class="icon m-r-2"><i class="fas fa-star"></i></span>                                    qualificatif
                            </span>
                            @endif
                            @if($b->is_selection == 1)
                            <span class="tag is-danger is-rounded">
                                <span class="icon m-r-2"><i class="fas fa-exclamation-circle"></i></span>                                    sur sélection
                            </span>
                            @endif
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="m-b-50 m-t-50">
            <p class="is-size-5"><b>Minimes</b> (2006/2005)</p>
            <div class="columns m-t-5">
                @foreach ($minimes as $m)
                    <div class="column is-one-quarter">
                        <div class="has-background-white-bis border p-t-10 p-b-10 p-l-10 p-r-10">
                            <p class="has-text-weight-bold">{{$m->nom}}</p>
                            <hr class="m-t-10 m-b-10">
                            <p>{{\Carbon\Carbon::parse($m->date)->format('d/m/Y')}}</p>
                            <p class="is-size-7 is-italic m-t-10">Niveau : {{$m->niveau}}</p>
                            <p class="is-size-7 is-italic m-b-10">Lieu : {{$m->lieu}}</p>
                            @if($m->is_qualificatif == 1)
                            <span class="tag is-success is-rounded m-r-5">
                                <span class="icon m-r-2"><i class="fas fa-star"></i></span>                                    qualificatif
                            </span>
                            @endif
                            @if($m->is_selection == 1)
                            <span class="tag is-danger is-rounded">
                                <span class="icon m-r-2"><i class="fas fa-exclamation-circle"></i></span>                                    sur sélection
                            </span>
                            @endif
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="m-b-50 m-t-50">
            <p class="is-size-5"><b>Cadets</b> (2004/2003)</p>
            <div class="columns m-t-5">
                @foreach ($cadets as $c)
                    <div class="column is-one-quarter">
                        <div class="has-background-white-bis border p-t-10 p-b-10 p-l-10 p-r-10">
                            <p class="has-text-weight-bold">{{$c->nom}}</p>
                            <hr class="m-t-10 m-b-10">
                            <p>{{\Carbon\Carbon::parse($c->date)->format('d/m/Y')}}</p>
                            <p class="is-size-7 is-italic m-t-10">Niveau : {{$c->niveau}}</p>
                            <p class="is-size-7 is-italic m-b-10">Lieu : {{$c->lieu}}</p>
                            @if($c->is_qualificatif == 1)
                            <span class="tag is-success is-rounded m-r-5">
                                <span class="icon m-r-2"><i class="fas fa-star"></i></span>                                    qualificatif
                            </span>
                            @endif
                            @if($c->is_selection == 1)
                            <span class="tag is-danger is-rounded">
                                <span class="icon m-r-2"><i class="fas fa-exclamation-circle"></i></span>                                    sur sélection
                            </span>
                            @endif
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        {{-- <div class="m-b-50 m-t-50">
            <p class="is-size-5"><b>Juniors</b></p>
        </div>
        <div class="m-b-50 m-t-50">
            <p class="is-size-5"><b>Seniors</b></p>
        </div>
        <div class="m-b-50 m-t-50">
            <p class="is-size-5"><b>Vétérans</b></p>
        </div> --}}
        

        
    </article>
</section>

@endsection