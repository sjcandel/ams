@extends('layouts.layouts-app.master')
@section('content') @if (session('status'))
<div class="notification is-success" role="alert">
    {{ session('status') }}
</div>
@endif

<div class="columns">
    <div class="column is-one-third is-offset-one-third m-t-100 m-b-100">
        <div class="card">
            <div class="card-content">
                <h1 class="title">Réinitialiser le mot de passe</h1>
                <form action="{{ route('password.request') }}" method="POST" role="form">
                    @csrf

                    <input type="hidden" name="token" value="{{ $token }}">
                    <div class="field">
                        <label for="email" class="label">Adresse e-mail</label>
                        <p class="control">
                            <input class="input {{$errors->has('email') ? 'is-danger' : ''}}" type="text" name="email" id="email" value="{{old('email')}}"
                                required>
                        </p>
                        @if ($errors->has('email'))
                        <p class="help is-danger">{{$errors->first('email')}}</p>
                        @endif
                    </div>

                    <div class="columns">
                        <div class="column">
                            <div class="field">
                                <label for="password" class="label">Mot de passe</label>
                                <p class="control">
                                    <input class="input {{$errors->has('password') ? 'is-danger' : ''}}" type="password" name="password" id="password" required>
                                </p>
                                @if ($errors->has('password'))
                                <p class="help is-danger">{{$errors->first('password')}}</p>
                                @endif
                            </div>
                        </div>
                        <div class="column">
                            <div class="field">
                                <label for="password_confirm" class="label">Confirmation</label>
                                <p class="control">
                                    <input class="input" type="password" name="password_confirmation" id="password_confirm" required>
                                </p>
                            </div>
                        </div>
                    </div>
                    <button class="button is-primary is-outlined is-fullwidth m-t-30">Réinitialiser</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
