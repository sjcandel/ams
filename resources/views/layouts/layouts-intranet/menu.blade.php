<div class="side-menu" id="side-menu">
  <aside class="menu">

    <p class="menu-label">
      <i class="fas fa-home m-r-5"></i><a href="{{ route('intranet.dashboard')}}" class="{{ Nav::isRoute('intranet.dashboard', $activeClass = "
        is-active ") }}">Accueil</a>
    </p>

    <p class="menu-label">
      <i class="fas fa-desktop m-r-5"></i>Gestion du site
    </p>
    <ul class="menu-list">
      <li><a href="{{route('actualites.index')}}" class="{{ Nav::isResource('actualites', 'intranet', $activeClass = " is-active
          ") }}">Actualités</a>
        <li><a href="{{route('calendrier.index')}}" class="{{ Nav::isResource('calendrier', 'intranet', $activeClass = " is-active
            ") }}">Calendrier</a></li>
        <li><a href="{{route('resultats.index')}}" class="{{ Nav::isResource('resultats', 'intranet', $activeClass = " is-active
            ") }}">Résultats</a></li>
        <li><a href="{{route('galerie.index')}}" class="{{ Nav::isResource('galerie', 'intranet', $activeClass = " is-active
            ") }}">Galerie</a></li>
    </ul>
    @if (Laratrust::can(['read-judokas', 'read-frais']))
    <p class="menu-label">
      <i class="fas fa-cogs m-r-5"></i>Gestion du club
    </p>@endif
    <ul class="menu-list">
      @if (Laratrust::can('read-judokas'))
      <li><a href="{{route('judokas.index')}}" class="{{ Nav::isResource('judokas', 'intranet', $activeClass = " is-active
          ") }}">Licenciés</a></li>@endif @if (Laratrust::can('read-frais'))
      <li><a href="{{route('fiches-de-frais.index')}}" class="{{ Nav::isResource('fiches-de-frais', 'intranet', $activeClass = "
          is-active ") }}">Fiches de frais</a></li>@endif
    </ul>

    @if (Laratrust::hasRole(['superadministrator','administrator']))
    <p class="menu-label">
      <i class="fas fa-users m-r-5"></i>Utilisateurs
    </p>@endif
    <ul class="menu-list">
      @if (Laratrust::can('read-utilisateurs'))
      <li><a href="{{ route('users.index') }}" class="{{ Nav::isResource('users', 'intranet', $activeClass = " is-active
          ") }}">Utilisateurs</a>
      </li>@endif @if (Laratrust::hasRole('superadministrator'))
      <li><a href="{{ route('roles.index')}}" class="{{ Nav::isResource('roles', 'intranet', $activeClass = " is-active
          ") }}">Roles</a></li>
      <li><a href="{{ route('permissions.index')}}" class="{{ Nav::isResource('permissions', 'intranet', $activeClass = " is-active
          ") }}">Permissions</a></li>@endif
    </ul>

    <hr class="is-hidden-desktop">
    <div class="is-hidden-desktop">
      <a class="navbar-item" href="{{ route('home') }}">
        <span class="icon m-r-2">
          <i class="fas fa-home"></i>
        </span>
        Retour au site
      </a>
      <a href="{{route('logout')}}" class="navbar-item" onclick="event.preventDefault();
                           document.getElementById('logout-form').submit();">
        <span class="icon m-r-2">
          <i class="fas fa-sign-out-alt"></i>
        </span>
        Déconnexion
      </a>
      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        {{ csrf_field() }}
      </form>
    </div>
  </aside>
</div>