<nav class="navbar is-fixed-top has-background-primary p-l-50 p-r-50">

  <div class="navbar-brand">
    <a class="navbar-item has-text-white">
      <p class="is-hidden-mobile"><strong>Arts Martiaux Schweighouse</strong>&nbsp; - Administration</p>
    </a>

    <a role="button" class="navbar-burger has-text-white" aria-label="menu" aria-expanded="false" id="menu-slideout-button">
        <span aria-hidden="true"></span>
        <span aria-hidden="true"></span>
        <span aria-hidden="true"></span>
      </a> {{-- <a class="navbar-item is-hidden-desktop has-text-white" id="menu-slideout-button">
      <span class="icon"><i class="fas fa-bars"></i></span>
    </a> --}}
  </div>

  <div class="navbar-menu">
    <div class="navbar-end">
      <div class="dropdown is-hoverable is-right navbar-item has-text-white" id="admin-dropdown-btn">
        <div class="dropdown-trigger">
          <div aria-haspopup="true" aria-controls="dropdown-menu">
            <span><b>{{$salutations}},</b> {{Auth::user()->prenom}}</span>
            <span class="icon is-small">
              <i class="fas fa-angle-down" aria-hidden="true"></i>
            </span>
          </div>
        </div>
        <div class="dropdown-menu" id="dropdown-menu" role="menu">
          <div class="dropdown-content">
            <div class="avatar-round-big">
              <img src="/uploads/assets/photos/users/{{ Auth::user()->photo }}">
            </div>
            <hr class="dropdown-divider">
            <a class="navbar-item dropdown-item p-l-40" href="{{ route('showProfile') }}">
              <span class="icon m-r-2">
                <i class="fas fa-user"></i>
              </span>
              Profil
            </a>
            <a class="navbar-item dropdown-item p-l-40" href="{{ route('home') }}">
              <span class="icon m-r-2">
                <i class="fas fa-home"></i>
              </span>
              Retour au site
            </a>
            <a href="{{route('logout')}}" class="navbar-item dropdown-item p-l-40" onclick="event.preventDefault();
                         document.getElementById('logout-form').submit();">
              <span class="icon m-r-2">
                <i class="fas fa-sign-out-alt"></i>
              </span>
              Déconnexion
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
              {{ csrf_field() }}
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

</nav>