<nav class="navbar is-fixed-top">
  <div class="container">
    <div class="navbar-brand">
      <a href="{{route('home')}}" class="navbar-item">
        <strong>Arts Martiaux Schweighouse</strong>
      </a>
      <a role="button" class="navbar-burger" aria-label="menu" aria-expanded="false" id="menu-slideout-button">
          <span aria-hidden="true"></span>
          <span aria-hidden="true"></span>
          <span aria-hidden="true"></span>
        </a>
    </div>
    <div class="navbar-menu">
      <div class="navbar-end">
        <div class="navbar-item has-dropdown is-hoverable">
          <a class="navbar-link">
            Club
          </a>
          <div class="navbar-dropdown is-right">
            <a class="navbar-item" href="{{route('club.presentation')}}">
              Présentation
            </a>
            {{-- <a class="navbar-item" href="{{route('club.disciplines')}}">
              Disciplines
            </a> --}}
            <a class="navbar-item" href="{{route('club.equipe')}}">
              Equipe
            </a>
          </div>
        </div>
        <div class="navbar-item has-dropdown is-hoverable">
          <a class="navbar-link">
              Horaires & Tarifs
          </a>
          <div class="navbar-dropdown is-right">
            <a class="navbar-item" href="{{route('horaires-tarifs.horaires')}}">
                Horaires
            </a>
            <a class="navbar-item" href="{{route('horaires-tarifs.tarifs')}}">
                Tarifs
            </a>
          </div>
        </div>
        <div class="navbar-item has-dropdown is-hoverable">
          <a class="navbar-link">
              Compétitions
          </a>
          <div class="navbar-dropdown is-right">
            <a class="navbar-item" href="{{route('competitions.calendrier')}}">
                Calendrier
            </a>
            <a class="navbar-item" href="{{route('competitions.resultats')}}">
                Résultats
            </a>
          </div>
        </div>
        <a class="navbar-item" href="{{route('galerie')}}">
            Galerie
        </a> {{-- <a href="{{route('home')}}" class="navbar-item">
            Contact
        </a> --}}
      </div>
    </div>
  </div>
</nav>

<div class="side-menu is-hidden-desktop" id="side-menu">
  <aside class="menu">
    <p class="menu-label">
      <a href="{{route('home')}}">Accueil</a>
    </p>
    <p class="menu-label">
      Club
    </p>
    <ul class="menu-list">
      <li><a href="{{route('club.presentation')}}">Présentation</a></li>
      {{-- <li><a href="{{route('club.disciplines')}}">Disciplines</a></li> --}}
      <li><a href="{{route('club.equipe')}}">Equipe</a></li>
    </ul>
    <p class="menu-label">
      Horaires & Tarifs
    </p>
    <ul class="menu-list">
      <li><a href="{{route('horaires-tarifs.horaires')}}">Horaires</a></li>
      <li><a href="{{route('horaires-tarifs.tarifs')}}">Tarifs</a></li>
    </ul>
    <p class="menu-label">
      Compétitions
    </p>
    <ul class="menu-list">
      <li><a href="{{route('competitions.calendrier')}}">Calendrier</a></li>
      <li><a href="{{route('competitions.resultats')}}">Résultats</a></li>
    </ul>
    <p class="menu-label">
      <a href="{{route('galerie')}}">
          Galerie
      </a>
    </p>
  </aside>
</div>