@extends('layouts.master') 
@section('header')
<title>@yield('pageTitle')</title>
<meta property="og:title" content="@yield('pageTitle')" />
<meta name="description" content="@yield('pageDescription')" />
<meta property="og:description" content="@yield('pageDescription')" />
@endsection
 
@section('body')

<body class="has-navbar-fixed-top">
    @include('layouts.layouts-app.nav')
    <div id="app">
        @yield('content')
    </div>
    <footer class="has-background-grey-darker has-text-grey-light">
        <section class="section">
            <div class="container has-text-centered m-b-60">
                <div class="columns is-horizontal-center is-mobile">
                    <a href="http://judo67.fr/" target="_blank" rel="nofollow" class="m-r-20">@Comité Judo 67</a>
                    <a href="http://www.judograndest.fr/" target="_blank" rel="nofollow" class="m-r-20">@Ligue Grand-Est de Judo</a>
                    <a href="https://www.ffjudo.com/" target="_blank" rel="nofollow" class="m-r-20">@Fédération Française de Judo</a>
                    <a href="https://techniques-de-judo.fr/" target="_blank">@Techniques de Judo</a>
                    {{-- <a href="https://www.facebook.com/difuriacoaching/">Difuria Coaching></a> --}}
                </div>
            </div>

            <div class="container has-text-centered">
                <p>©2018 - Arts Martiaux Schweighouse </p>
                <p class="m-b-20">Mentions légales</p>
                <a href="{{ route('intranet.dashboard') }}">Administration</a>
            </div>
        </section>
    </footer>

    <script src="{{ asset('/js/app.js') }}"></script>
    @yield('scripts')
</body>
@endsection