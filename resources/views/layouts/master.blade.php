<!DOCTYPE html>
<html prefix="og: http://ogp.me/ns#" lang="{{ str_replace('_', '-', app()->getLocale()) }}" class=" has-background-white-ter">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}"> @yield('header')
  <meta property="og:locale" content="fr_FR" />
  <meta property="og:type" content="website" />
  <meta property="og:url" content="http://judo-schweighouse.fr/" />

  <link rel="icon" type="image/png" href="/uploads/assets/favicon.png" sizes="16x16">

  <link rel="canonical" href="http://judo-schweighouse.fr/" />

  <!-- Styles -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp"
    crossorigin="anonymous">
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">

  <!-- Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-125721100-1"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-125721100-1');
  </script>


  <script type="application/ld+json">
    {
      "@context": "http://schema.org",
      "@type": "Organization",
      "name": "Arts Martiaux Schweighouse",
      "url": "http://judo-schweighouse.fr",
      "address" : {
        "@type" : "PostalAddress",
        "streetAddress" : "10 rue des sports",
        "addressLocality" : "Schweighouse sur Moder",
        "postalCode" : "67590"
      },
      "sameAs": [
        "https://www.facebook.com/AMSchweighouse/"
      ]
    }
  </script>

</head>

@yield('body')


</html>
{{--
<script src="{{ asset('/js/app.js') }}"></script>
@yield('scripts') --}}