require('./bootstrap');

window.Vue = require('vue');
import Buefy from 'buefy';

Vue.use(Buefy, {
    defaultIconPack: 'fas'
});

// require('./intranet');

const adminSlideButton = document.getElementById('menu-slideout-button');
// const adminDropdown = document.getElementById('admin-dropdown-btn');

adminSlideButton.onclick = function() {
    this.classList.toggle('is-active');
    document.getElementById('side-menu').classList.toggle('is-active');
};

// adminDropdown.onclick = function() {
//     console.log(this);
//     this.classList.toggle('is-active');
//     document.getElementById('admin-dropdown').classList.toggle('is-active');
// };
