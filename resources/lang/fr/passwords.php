<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Les mots de passe doivent avoir au moins six caractères et doivent être identiques.',
    'reset' => 'Votre mot de passe a été réinitialisé',
    'sent' => 'Un lien pour réinitialiser votre mot de passe vous a été envoyé',
    'token' => 'This password reset token is invalid',
    'user' => "Cette adresse e-mail ne correspond à aucun utilisateur",

];
