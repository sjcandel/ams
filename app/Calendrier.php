<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Calendrier extends Model
{
    protected $with = ['user'];
    protected $fillable = ['admin_validation', 'nom', 'lieu', 'date', 'horaires', 'niveau', 'categorie', 'is_qualificatif', 'is_selection'];
    public function user() {
        return $this->hasOne(User::class, 'id', 'auteur');
    }
}
