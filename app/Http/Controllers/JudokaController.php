<?php

namespace App\Http\Controllers;

use Auth;
use App\Judoka;
use Dompdf\Dompdf;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use DateTime;

class JudokaController extends Controller
{

    /** INDEX  **/
    public function index()
    {
        if (!Auth::user()->hasPermission('read-judokas')) return redirect()->route('intranet');
        $judokas = Judoka::orderBy('nom')->where('status', 1)->get();
        $archives = Judoka::orderBy('nom')->where('status', 0)->get();
        // $judokas = Judoka::orderBy('nom')->where('status', 1)->paginate(20);
        // $archives = Judoka::orderBy('nom')->where('status', 0)->paginate(40);
        
        $judokasCount = DB::table('judokas')->where('status', 1)->count();
        $archivesCount = DB::table('judokas')->where('status', 0)->count();

        // $now = new DateTime();
        // $year = $now->format('Y');
        // if($now->format('m') < 8) {
        //     $currentYear = ($year-1) . '/' . $year;
        // } else {
        //     $currentYear = $year . '/' . ($year+1);
        // }

        return view('intranet.judokas.index')
            ->withJudokas($judokas)
            ->withArchives($archives)
            ->withJudokasCount($judokasCount)
            ->withArchivesCount($archivesCount);
            // ->withCurrentYear($currentYear);
    }

    /** CREATE  **/
    public function create()
    {
        if (!Auth::user()->hasPermission('create-judokas')) return redirect()->route('intranet');
        return view('intranet.judokas.create');
    }

    /** STORE  **/
    public function store(Request $request)
    {
        if (!Auth::user()->hasPermission('create-judokas')) return redirect()->route('intranet');
        $this->validate($request, [
            'nom' => 'required',
            'prenom' => 'required',
            'file' => 'filled|file|mimetypes:image/jpeg,image/png',
            'date_naissance' => 'required|date',
            'licence' => 'required',
            'dojo' => 'required',
            'grade' => 'required'
        ]); 

        $judoka = new Judoka();
        $judoka->status = 1;
        $judoka->nom = $request->nom;
        $judoka->prenom = $request->prenom;
        $judoka->date_naissance = $request->date_naissance;
        $judoka->licence = $request->licence;
        $judoka->dojo = $request->dojo;
        $judoka->grade = $request->grade;
        $judoka->telephone = $request->telephone;
        $judoka->portable = $request->portable;
        $judoka->email = $request->email;
        $judoka->adresse_rue = $request->adresse_rue;
        $judoka->adresse_cp = $request->adresse_cp;
        $judoka->adresse_ville = $request->adresse_ville;

        // ASSIGNATION CATEGORIE D'AGE
        
        // $date_naissance = $request->date_naissance;
        // if (2011-01-01 < $date_naissance < 2012-12-31) {
        //     $judoka->categorie = "mini-poussin"
        // }


        // if($request->has('file')) {
        //     $judoka->photo = $request->file->getClientOriginalName();
        //     $request->file->storeAs('assets/photos/judokas', $judoka->photo, 'public');
        // }

        // RENOMMER PHOTO
        if($request->has('file')) {
            $photoName = $judoka->id.'-'.$judoka->nom.$judoka->prenom;
            $photoExtension = $request->file->getClientOriginalExtension();

            $judoka->photo = $photoName.'.'.$photoExtension;
            $request->file->storeAs('assets/photos/judokas', $photoName.'.'.$photoExtension, 'public');
        }

        $judoka->save();

        return redirect()->route('judokas.show', ['id'=>$judoka->id]);
    }

    /** SHOW  **/
    public function show($id)
    {
        if (!Auth::user()->hasPermission('read-judokas')) return redirect()->route('intranet');
        $judoka = Judoka::findOrFail($id);
        return view('intranet.judokas.show')->withJudoka($judoka);
    }

    /** EDIT  **/
    public function edit($id)
    {
        if (!Auth::user()->hasPermission('update-judokas')) return redirect()->route('intranet');
        $judoka = Judoka::findOrFail($id);
        return view('intranet.judokas.edit')->withJudoka($judoka);
    }

    /** UPDATE  **/
    public function update(Request $request, $id)
    {
        if (!Auth::user()->hasPermission('update-judokas')) return redirect()->route('intranet');
        $this->validate($request, [
            'file' => 'nullable|file|mimetypes:image/jpeg,image/jpg,image/png',
            'date_naissance' => 'filled|date'
        ]);

        $judoka = Judoka::findOrFail($id);
        $judoka->nom = $request->nom;
        $judoka->prenom = $request->prenom;
        $judoka->date_naissance = $request->date_naissance;
        $judoka->licence = $request->licence;
        $judoka->dojo = $request->dojo;
        $judoka->grade = $request->grade;
        $judoka->telephone = $request->telephone;
        $judoka->portable = $request->portable;
        $judoka->email = $request->email;
        $judoka->adresse_rue = $request->adresse_rue;
        $judoka->adresse_cp = $request->adresse_cp;
        $judoka->adresse_ville = $request->adresse_ville;

        // RENOMMER PHOTO
        if($request->has('file')) {
            $photoName = $judoka->id.$judoka->nom.$judoka->prenom;
            $photoExtension = $request->file->getClientOriginalExtension();

            $judoka->photo = $photoName.'.'.$photoExtension;
            $request->file->storeAs('assets/photos/judokas', $photoName.'.'.$photoExtension, 'public');
        }

        $judoka->save();

        return redirect()->route('judokas.show', ['id'=>$judoka->id]);
    }

    /** DESTROY  **/
    public function destroy($id)
    {
        if (!Auth::user()->hasPermission('delete-judokas')) return redirect()->route('intranet');
        $judoka = Judoka::findOrFail($id);
        $judoka->delete();
        Storage::delete('public/assets/'.$judoka->photo);
        return view('intranet.judokas.index');
    }

    /**
     * Affiche la fiche du $judoka au format PDF
     * @param Judoka $judoka le judoka à afficher
     * @return void
     */
    public function pdf(Judoka $judoka) {
        if (!Auth::user()->hasPermission('read-judokas')) return redirect()->route('intranet');
        $html = view('intranet.judokas.pdf', [
            'judoka' => $judoka,
            'picture_exists' => Storage::exists('public/assets/photos/judokas/' . $judoka->photo),
        ]);
        $dompdf = new Dompdf();
        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();
        $dompdf->stream('judoka-' . $judoka->id . '.pdf', [
            'compress' => 1,
            'Attachment' => 0,
        ]);
    }

    

    /** DE-ARCHIVAGE MANUEL **/
    public function unarchive(Judoka $id) {
        if (!Auth::user()->hasPermission('update-judokas')) {
            $judoka = Judoka::findOrFail($id);
            $judoka->status = 1;
            $judoka->save();

            // $judoka->update([
            //     'status' => 1,
            // ]);
        }
        return redirect()->route('judokas.index');
    }
}
