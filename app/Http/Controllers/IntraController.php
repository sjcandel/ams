<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class IntraController extends Controller
{
    public function index() {
        return redirect()->route('intranet.dashboard');
    }
    
    public function dashboard() {
        $judokas = DB::table('judokas')->count();
        $actualites = DB::table('actualites')->where('admin_validation', 1)->count();
        $calendrier = DB::table('calendriers')->count();
        $photos = DB::table('photos')->count();
 
        return view('intranet.dashboard')
        ->withJudokas($judokas)
        ->withActualites($actualites)
        ->withCalendrier($calendrier)
        ->withPhotos($photos);
    }

}
