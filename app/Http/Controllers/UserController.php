<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use DB;
use Session;
use Hash;
use Auth;
// use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!Auth::user()->hasPermission('read-utilisateurs')) return redirect()->route('intranet');
        $users = User::orderBy('id')->paginate(20);
        return view('intranet.users.index')->withUsers($users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!Auth::user()->hasPermission('create-utilisateurs')) return redirect()->route('intranet');
        return view('intranet.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!Auth::user()->hasPermission('create-utilisateurs')) return redirect()->route('intranet');
        $this->validate($request, [
            'prenom' => 'required|max:255',
            'nom' => 'required|max:255',
            'email' => 'required|email|unique:users',
            'file' => 'filled|file|mimetypes:image/jpeg,image/jpg,image/png',
        ]);

        if ($request->filled('password')) {
            $password = trim($request->password);
        } else {
            $password = Hash::make(str_random(8));
        }

        $user = new User();
        $user->prenom = $request->prenom;
        $user->nom = $request->nom;
        $user->email = $request->email;
        $user->password = Hash::make($password);

        if($request->has('file')) {
            $user->photo = $request->file->getClientOriginalName();
            $request->file->storeAs('assets/photos/users', $user->photo, 'public');
        }

        $user->save();

        return redirect()->route('users.show', ['id'=>$user->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (!Auth::user()->hasPermission('read-utilisateurs')) return redirect()->route('intranet');
        $user = User::where('id', $id)->with('roles')->first();
        return view("intranet.users.show")->withUser($user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!Auth::user()->hasPermission('update-utilisateurs')) return redirect()->route('intranet');
        $roles = Role::all();
        $user = User::where('id', $id)->with('roles')->first();
        return view("intranet.users.edit")->withUser($user)->withRoles($roles);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!Auth::user()->hasPermission('update-utilisateurs')) return redirect()->route('intranet');
        $this->validate($request, [
            'file' => 'nullable|file|mimetypes:image/jpeg,image/jpg,image/png',
        ]);
        
        if($request->password_options == 'auto') {
            $password = Hash::make(str_random(8));
        } 
        elseif($request->password_options == 'manual') {
            $user->password = Hash::make($request->password);
        }

        $user = User::findOrFail($id);
        $user->prenom = $request->prenom;
        $user->nom = $request->nom;
        $user->email = $request->email;
        //$user->password = $request->password;
        if($request->has('file')) {
            $user->photo = $request->file->getClientOriginalName();
            $request->file->storeAs('assets/photos/users', $user->photo, 'public');
        }

        $user->save();

        $user->syncRoles(explode(',', $request->roles));

        return redirect()->route('users.show', ['id'=>$user->id]);

        // if($user->save){
        //     return redirect()->route('users.show', $id); 
        // } else {
        //     Session::flash('danger', 'Sorry, a problem occured while creating this user.');
        //     return redirect()->route('users.edit', $id);
        // }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!Auth::user()->hasPermission('delete-utilisateurs')) return redirect()->route('intranet');
        $user = User::findOrFail($id);
        $user->delete();
        
        return redirect()->route('users.index'); 
    }
}
