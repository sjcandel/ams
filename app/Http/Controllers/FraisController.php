<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;

class FraisController extends Controller
{
    /** INDEX **/
    public function index() {
        if (!Auth::user()->hasPermission('read-frais')) return redirect()->route('intranet');
        return view('intranet.fiches-de-frais.index');
    }

    /** CREATE **/
    public function create() {
        if (!Auth::user()->hasPermission('create-frais')) return redirect()->route('intranet');
        return view('intranet.fiches-de-frais.create');
    }

    /** STORE **/
    public function store(Request $request) {
        if (!Auth::user()->hasPermission('create-frais')) return redirect()->route('intranet');
        //
    }

    /** SHOW **/
    public function show($id) {
        if (!Auth::user()->hasPermission('read-frais')) return redirect()->route('intranet');
        //
    }

    /** EDIT **/
    public function edit($id) {
        if (!Auth::user()->hasPermission('update-frais')) return redirect()->route('intranet');
        //
    }

    /** UPDATE **/
    public function update(Request $request, $id) {
        if (!Auth::user()->hasPermission('update-frais')) return redirect()->route('intranet');
        //
    }

    /** DESTROY **/
    public function destroy($id) {
        if (!Auth::user()->hasPermission('delete-frais')) return redirect()->route('intranet');
        //
    }
}
