<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Actualite;
use App\Calendrier;
use App\Galerie;
use App\Album;
use DB;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function home() {
        $actualites = Actualite::where('admin_validation', 1)->orderBy('created_at','desc')->take(9)->get();
        return view('home')->withActualites($actualites);
    }

    public function presentation() {
        return view('app.presentation');
    }

    public function equipe() {
        return view('app.equipe');
    }
    public function horaires() {
        return view('app.horaires');
    }
    public function tarifs() {
        return view('app.tarifs');
    }
    public function calendrier() {

        $minipoussins =
         Calendrier::where('categorie', 'minipoussins')
                    ->where('date', '>=', NOW())
                    ->orderBy('date')
                    ->get();
        $poussins =
         Calendrier::where('categorie', 'poussins')
                    ->where('date', '>=', NOW())
                    ->orderBy('date')
                    ->get();
        $benjamins =
         Calendrier::where('categorie', 'benjamins')
                    ->where('date', '>=', NOW())
                    ->orderBy('date')
                    ->get();
        $minimes =
         Calendrier::where('categorie', 'minimes')
                    ->where('date', '>=', NOW())
                    ->orderBy('date')
                    ->get();
        $cadets =
         Calendrier::where('categorie', 'cadets')
                    ->where('date', '>=', NOW())
                    ->orderBy('date')
                    ->get();
        $juniors =
         Calendrier::where('categorie', 'juniors')
                    ->where('date', '>=', NOW())
                    ->orderBy('date')
                    ->get();
        $seniors =
         Calendrier::where('categorie', 'seniors')
                    ->where('date', '>=', NOW())
                    ->orderBy('date')
                    ->get();
        $veterans =
         Calendrier::where('categorie', 'veterans')
                    ->where('date', '>=', NOW())
                    ->orderBy('date')
                    ->get();
        
        return view('app.calendrier')
            ->withMinipoussins($minipoussins)
            ->withPoussins($poussins)
            ->withBenjamins($benjamins)
            ->withMinimes($minimes)
            ->withCadets($cadets)
            ->withJuniors($juniors)
            ->withSeniors($seniors)
            ->withVeterans($veterans);

        
    }
    public function resultats() {
        return view('app.resultats');
    }
    public function galerie() {
        $albums = Album::orderBy('date', 'desc')->get();
        return view('app.galerie')->withAlbums($albums);
    }

    public function galerieDetails($id)
    {
        $album = Album::findOrFail($id);
        $photos = Album::find($id)->photos;
        
        return view('app.galerieDetails')
            ->withAlbum($album)
            ->withPhotos($photos);
    }
}
