<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Actualite;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use View;
use Illuminate\Support\Facades\DB;

class ActualiteController extends Controller
{


    /** INDEX  **/
    public function index() {
        if (!Auth::user()->hasPermission('read-actualites')) return redirect()->route('intranet');

        $actualites = Actualite::orderBy('created_at', 'desc')
            ->where('admin_validation', 1)
            ->paginate('10');
        $actualitesCount = DB::table('actualites')->where('admin_validation', 1)->count();
        $waiting = Actualite::where('admin_validation', 0)
            ->get();
        $waitingCount = DB::table('actualites')->where('admin_validation', 0)->count();
        // $draft = Actualite::where('status', 0)
        //     ->where('auteur', auth()->user()->id)
        //     ->get();
        // $draftCount = DB::table('actualites')->where('status', 0)->where('auteur', auth()->user()->id)->count();
        return view('intranet.actualites.index')
            ->withActualites($actualites)
            ->withActualitesCount($actualitesCount)
            ->withWaiting($waiting)
            ->withWaitingCount($waitingCount);
            // ->withDraft($draft)
            // ->withDraftCount($draftCount);
    }

    /** CREATE  **/
    public function create() {
        if (!Auth::user()->hasPermission('create-actualites')) return redirect()->route('intranet');
        return view('intranet.actualites.create');
    }

    /** STORE **/
    public function store(Request $request) {
        if (!Auth::user()->hasPermission('create-actualites')) return redirect()->route('intranet');
        $this->validate($request, [
            'title' => 'required|max:255',
            'content' => 'required',
            'file' => 'filled|file|mimetypes:image/jpeg,image/jpg,image/png',
            // 'status' => 'required|boolean',
        ]);

        $actualite = new Actualite();
        $actualite->title = ucfirst(strtolower($request->title));
        $actualite->content = $request->content;
        $actualite->image = $request->file;
        // $actualite->status = $request->input('status', 0);
        // $actualite->published_at = Carbon::now();

        // Saving file
        if($request->has('file')) {
            $actualite->image = $request->file->getClientOriginalName();
            $request->file->storeAs('assets/photos/actualites', $actualite->image, 'public');
        }

        // Saving status
        $user = auth()->user();
        $actualite->auteur = $user->id;
        if ($user->hasRole('superadministrator|administrator|moderator')) {
            $actualite->admin_validation = 1;
        }
        else {
            $actualite->admin_validation = 0;
        }
        
        // $user = auth()->user();
        // $actualite->auteur = $user->id;
        // if ($request->status == 1) {
        //     if ($user->hasRole('superadministrator|administrator|moderator')) {
        //         $actualite->admin_validation = 1;
        //     }
        //     else {
        //         $actualite->admin_validation = 0;
        //     }
        // }
        $actualite->save();

        return redirect()->route('actualites.show', ['id'=>$actualite->id]);
    }

    /** ******************** **/
    /** SHOW  **/
    /** ******************** **/
    public function show(Actualite $actualite) {
        $user = auth()->user();

        // if it's a draft, only the author can see it
        // if ($actualite->status == 0 && $actualite->auteur != $user->id) {
        //     return redirect()->route('intranet');
        // }

        if (!$user->hasPermission('read-actualites')) {
            return redirect()->route('intranet');
        }

        return view('intranet.actualites.show')->withActualite($actualite);
    }

    /** ******************** **/
    /** EDIT **/
    /** ******************** **/
    public function edit(Actualite $actualite) {
        $user = auth()->user();

        // if it's a draft, only the author can update it
        // if ($actualite->status == 0 && $actualite->auteur != $user->id) {
        //     return redirect()->route('intranet');
        // }

        if (!$user->hasPermission('update-actualites')) {
            return redirect()->route('intranet');
        }

        // if (!Auth::user()->hasPermission('update-actualites')) {
        //     return redirect()->route('intranet');
        // } else {
        //     if (Auth::user()->hasRole('secretary|trainer|accountant|contributor')) {
        //         if  (Auth::user()->canAndOwns('edit-post', $id)) {
        //             $actualite = Actualite::findOrFail($id);
        //             return view('intranet.actualites.edit')->withActualite($actualite);
        //         }
        //     }
        // }

        return view('intranet.actualites.edit')->withActualite($actualite);
    }

    /** ******************** **/
    /** UPDATE **/
    /** ******************** **/
    public function update(Request $request, $id) {

        if (!Auth::user()->hasPermission('update-actualites')) return redirect()->route('intranet');
        $this->validate($request, [
            'title' => 'required|max:255',
            'content' => 'required',
            'file' => 'filled|file|mimetypes:image/jpeg,image/jpg,image/png',
        ]);

        $actualite = Actualite::findOrFail($id);
        $actualite->title = ucfirst(strtolower($request->title));
        $actualite->content = $request->content;
        // $actualite->image = $request->file;

        // Saving file
        if($request->has('file')) {
            $actualite->image = $request->file->getClientOriginalName();
            $request->file->storeAs('assets/photos/actualites', $actualite->image, 'public');
        }

        
        $actualite->save();

        return redirect()->route('actualites.show', ['id'=>$actualite->id]);

        // $user = auth()->user();

        // if it's a draft, only the author can update it
        // if ($actualite->status == 0 && $actualite->auteur != $user->id) {
        //     return redirect()->route('intranet');
        // }

        // if (!$user->hasPermission('update-actualites')) return redirect()->route('intranet');
        // $this->validate($request, [
        //     'title' => 'filled',
        //     'content' => 'filled',
        //     'file' => 'nullable|file|mimetypes:image/jpeg,image/jpg,image/png',
        //     // 'status' => 'nullable|boolean',
        // ]);

        // $actualite->update([
        //     'title' => ucfirst(strtolower($request->title)),
        //     'content' => $request->content,
        //     'image' => $request->file,
        //     // 'status' => $request->input('status', 0),
        // ]);

        // // Saving file
        // if($request->has('file')) {
        //     $actualite->image = $request->file->getClientOriginalName();
        //     $request->file->storeAs('assets/photos/actualites', $actualite->image, 'public');
        // }

        // return redirect()->route('actualites.show', ['id' => $actualite->id]);
    }

    public function accept(Actualite $actualite) {
        $user = auth()->user();
        if ($user->hasRole('superadministrator|administrator|moderator')) {
            $actualite->update([
                'admin_validation' => 1,
            ]);
        }
        return redirect()->route('actualites.index');
    }

    public function reject(Actualite $actualite) {
        $user = auth()->user();
        if ($user->hasRole('superadministrator|administrator|moderator')
            || $user->id == $actualite->auteur
        ) {
            $actualite->update([
                'admin_validation' => 0,
                // 'status' => 0,
            ]);
        }
        return redirect()->route('actualites.index');
    }

    /** ******************** **/
    /** DELETE  **/
    /** ******************** **/
    public function delete(Actualite $actualite) {
        $user = auth()->user();

        // if it's a draft, only the author can see it
        // if ($actualite->status == 0 && $actualite->auteur != $user->id) {
        //     return redirect()->route('intranet');
        // }

        // if ($actualite->status != 0 && !$user->hasPermission('delete-actualites')) {
        //     return redirect()->route('intranet');
        // }

        if (!$user->hasPermission('delete-actualite')) {
            return redirect()->route('intranet');
        }

        $actualite->delete();

        return redirect()->route('intranet');
    }
}
