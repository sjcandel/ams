<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use DB;
use Hash;
use Auth;

class ProfileController extends Controller
{

    public function showProfile() {
        if (!Auth::user()->hasPermission('read-profile')) return redirect()->route('intranet');
        return view("intranet.profile.show");
    }

    public function showChangePasswordForm(){
        if (!Auth::user()->hasPermission('update-profile')) return redirect()->route('intranet');
        return view("intranet.profile.edit");
    }

    public function changePassword(Request $request){
        if (!Auth::user()->hasPermission('update-profile')) return redirect()->route('intranet');

        if (!(Hash::check($request->get('current-password'), Auth::user()->password))) {
            return redirect()->back()->with("error","Votre mot de passe actuel ne correspond pas à celui que vous venez d'entrer. Veuillez réessayer.");
        }
 
        if(strcmp($request->get('current-password'), $request->get('new-password')) == 0){
            return redirect()->back()->with("error","Votre nouveau mot de passe ne peut pas être le même que le précédent. Veuillez en choisir un autre.");
        }
 
        $validatedData = $request->validate([
            'current-password' => 'required',
            'new-password' => 'required|string|min:6|confirmed',
        ]);
 
        //Change Password
        $user = Auth::user();
        $user->password = bcrypt($request->get('new-password'));
        $user->save();
 
        return redirect()->back()->with("success","Mot de passe modifié avec succès !");
    }
    
}
