<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\Calendrier;
use Illuminate\Support\Facades\DB;

class CalendrierController extends Controller
{
    /** INDEX **/
    public function index() {
        if (!Auth::user()->hasPermission('read-calendrier')) return redirect()->route('intranet');
        $evenements = Calendrier::where('admin_validation', 1)->where('date', '>=', NOW())->orderBy('date')->get();
        // $evenements = Calendrier::where('admin_validation', 1)->where('date', '>=', NOW())->orderBy('date')->paginate(20);
        $evenementsCount = DB::table('calendriers')->where('admin_validation', 1)->where('date', '>=', NOW())->count();

        $archives = Calendrier::where('admin_validation', 1)->where('date', '<', NOW())->orderBy('date', 'desc')->get();
        // $archives = Calendrier::where('admin_validation', 1)->where('date', '<', NOW())->orderBy('date', 'desc')->paginate(20);
        $archivesCount = DB::table('calendriers')->where('admin_validation', 1)->where('date', '<', NOW())->count();

        $waiting = Calendrier::where('admin_validation', 0)->paginate(20);
        $waitingCount = DB::table('calendriers')->where('admin_validation', 0)->count();



        return view('intranet.calendrier.index')
            ->withEvenements($evenements)
            ->withEvenementsCount($evenementsCount)
            ->withWaiting($waiting)
            ->withArchives($archives)
            
            ->withArchivesCount($archivesCount)
            ->withWaitingCount($waitingCount);
    }

    /** CREATE **/
    public function create() {
        if (!Auth::user()->hasPermission('create-calendrier')) return redirect()->route('intranet');
        return view('intranet.calendrier.create');
    }

    /** STORE **/
    public function store(Request $request) {
        if (!Auth::user()->hasPermission('create-calendrier')) return redirect()->route('intranet');
        $this->validate($request, [
            'nom' => 'required|max:255',
            'lieu' => 'required|max:255',
            'date' => 'required|date',
            'horaires' => 'required',
            'niveau' => 'required',
            'categorie' => 'required',
            'is_selection' => 'filled|boolean',
            'is_qualificatif' => 'filled|boolean',
        ]);

        $calendrier = new Calendrier();
        $calendrier->nom = $request->nom;
        $calendrier->lieu = $request->lieu;
        $calendrier->date = $request->date;
        $calendrier->horaires = $request->horaires;
        $calendrier->niveau = $request->niveau;
        $calendrier->categorie = $request->categorie;
        $calendrier->is_selection = $request->input('is_selection', 0);
        $calendrier->is_qualificatif = $request->input('is_qualificatif', 0);

        $user = auth()->user();
        $calendrier->auteur = $user->id;

        
        $user = auth()->user();
        if ($user->hasRole('superadministrator|administrator|moderator')) {
            $calendrier->admin_validation = 1;
        }
        else {
            $calendrier->admin_validation = 0;
        }

        $calendrier->save();

        return redirect()->route('calendrier.show', ['id'=>$calendrier->id]);
    }

    /** SHOW **/
    public function show($id) {
        if (!Auth::user()->hasPermission('read-calendrier')) return redirect()->route('intranet');
        $evnt = Calendrier::findOrFail($id);
        return view('intranet.calendrier.show')->withEvnt($evnt);
    }

    /** EDIT **/
    public function edit($id) {
        if (!Auth::user()->hasPermission('update-calendrier')) return redirect()->route('intranet');
        $evnt = Calendrier::findOrFail($id);
        return view('intranet.calendrier.edit')->withEvnt($evnt);


        // TDOO : if date is over, can not edit
    }

    /** UPDATE **/
    public function update(Request $request, $id) {
        if (!Auth::user()->hasPermission('update-calendrier')) return redirect()->route('intranet');
        
        $this->validate($request, [
            'nom' => 'filled|max:255',
            'lieu' => 'filled|max:255',
            'date' => 'filled|date',
            'niveau' => 'required',
            'categorie' => 'required',
            'is_selection' => 'filled|boolean',
            'is_qualificatif' => 'filled|boolean',
        ]);

        $calendrier = Calendrier::findOrFail($id);
        $calendrier->nom = $request->nom;
        $calendrier->lieu = $request->lieu;
        $calendrier->date = $request->date;
        $calendrier->horaires = $request->horaires;
        $calendrier->niveau = $request->niveau;
        $calendrier->categorie = $request->categorie;
        $calendrier->is_selection = $request->input('is_selection', 0);
        $calendrier->is_qualificatif = $request->input('is_qualificatif', 0);

        $calendrier->save();

        return redirect()->route('calendrier.show', ['id'=>$calendrier->id]);
    }

    /** DESTROY **/
    public function destroy($id) {
        if (!Auth::user()->hasPermission('delete-calendrier')) return redirect()->route('intranet');
        //
    }

    public function accept(Calendrier $calendrier) {
        $user = auth()->user();
        if ($user->hasRole('superadministrator|administrator|moderator')) {
            $calendrier->update([
                'admin_validation' => 1,
            ]);
        }
        return redirect()->route('calendrier.index');
    }

    public function reject(Calendrier $calendrier) {
        $user = auth()->user();
        if ($user->hasRole('superadministrator|administrator|moderator')) {
            $calendrier->update([
                'admin_validation' => 0,
            ]);
        }
        return redirect()->route('calendrier.index');
    }
}
