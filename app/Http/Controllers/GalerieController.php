<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\Album;
use DB;

class GalerieController extends Controller
{
    /** INDEX **/
    public function index() {
        if (!Auth::user()->hasPermission('read-galerie')) return redirect()->route('intranet');

        $albums = Album::orderBy('date')->get();

        return view('intranet.galerie.index')->withAlbums($albums);
    }

    /** CREATE **/
    public function create() {
        if (!Auth::user()->hasPermission('create-galerie')) return redirect()->route('intranet');
        return view('intranet.galerie.create');
    }

    /** STORE **/
    public function store(Request $request) {
        if (!Auth::user()->hasPermission('create-galerie')) return redirect()->route('intranet');
        
        $this->validate($request, [
            'titre' => 'required|max:255',
            'lieu' => 'required',
            'date' => 'required|date',
            'files' => 'required',
            'files.*' => 'mimetypes:image/jpeg,image/png'
        ]);

        $album = new Album();
        $album->titre = $request->titre;
        $album->lieu = $request->lieu;
        $album->date = $request->date;

        $album->save();

        foreach($request->file('files') as $file) {
            $name = $file->getClientOriginalName();
            $file->storeAs('assets/photos/galerie', $name, 'public');

            DB::table('photos')->insert(
                ['nom' => $name, 
                'album_id' => $album->id]
            );
        }

        return redirect()->route('galerie.show', ['id'=>$album->id]);

    }

    /** SHOW **/
    public function show($id) {
        if (!Auth::user()->hasPermission('read-galerie')) return redirect()->route('intranet');
        $album = Album::findOrFail($id);
        $photos = Album::find($id)->photos;
        return view('intranet.galerie.show')->withAlbum($album)->withPhotos($photos);
    }

    /** EDIT **/
    public function edit($id) {
        if (!Auth::user()->hasPermission('update-galerie')) return redirect()->route('intranet');
        //
    }

    /** UPDATE **/
    public function update(Request $request, $id) {
        if (!Auth::user()->hasPermission('update-galerie')) return redirect()->route('intranet');
        //
    }

    /** DESTROY **/
    public function destroy($id) {
        if (!Auth::user()->hasPermission('delete-galerie')) return redirect()->route('intranet');
        //
    }
}
