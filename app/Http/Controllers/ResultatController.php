<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;

class ResultatController extends Controller
{
    /** INDEX **/
    public function index() {
        if (!Auth::user()->hasPermission('read-resultats')) return redirect()->route('intranet');
        return view('intranet.resultats.index');
    }

    /** CREATE **/
    public function create() {
        if (!Auth::user()->hasPermission('create-calendrier')) return redirect()->route('intranet');
        return view('intranet.resultats.create');
    }

    /** STORE **/
    public function store(Request $request) {
        if (!Auth::user()->hasPermission('create-calendrier')) return redirect()->route('intranet');
        //
    }

    /** SHOW **/
    public function show($id) {
        if (!Auth::user()->hasPermission('read-calendrier')) return redirect()->route('intranet');
        //
    }

    /** EDIT **/
    public function edit($id) {
        if (!Auth::user()->hasPermission('update-calendrier')) return redirect()->route('intranet');
        //
    }

    /** UPDATE **/
    public function update(Request $request, $id) {
        if (!Auth::user()->hasPermission('update-calendrier')) return redirect()->route('intranet');
        //
    }

    /** DESTROY **/
    public function destroy($id) {
        if (!Auth::user()->hasPermission('delete-calendrier')) return redirect()->route('intranet');
        //
    }
}
