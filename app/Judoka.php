<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Judoka extends Model
{
    protected $table = 'judokas';
    protected $fillable = [
        'nom',
        'prenom',
        'status',
        'photo',
        'date_naissance',
        'dojo',
        'grade',
        'licence',
        'telephone',
        'portable',
        'email',
        'adresse_rue',
        'adresse_cp',
        'adresse_ville',
    ];
}
