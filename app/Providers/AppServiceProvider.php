<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        $heure = date('G');
        switch ($heure) {
            case $heure < 8:
                $salutations = 'Oyasumi';
                break;
            case $heure < 12:
                $salutations =  'Ohayô';
                break;
            case $heure < 16:
                $salutations =  'Konnichiwa';
                break;
            case $heure < 20:
                $salutations =  'Konbanwa';
                break;
            case $heure > 20:
                $salutations =  'Oyasumi';
                break;
            default:
                $salutations =  'Konnichiwa';
        }
        View::share('salutations', $salutations);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
