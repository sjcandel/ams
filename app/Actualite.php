<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Actualite extends Model
{
    protected $with = ['user'];

    protected $fillable = ['admin_validation', 'status', 'title', 'content'];

    public function user() {
        return $this->hasOne(User::class, 'id', 'auteur');
    }
}
