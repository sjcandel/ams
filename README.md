`ams`
=====

## Prérequis

Pour pouvoir lancer ce projet, il faut impérativement avoir :

  - `composer` (voir https://getcomposer.org/)

  - une version de PHP >= 7.1.3

  - une version de Node >= v8.9.0 et de npm >= 5.5.1



## Mise en route

Une fois le dépôt clôné, il faut copier le fichier `.env.example` en un fichier
`.env`, dans lequel les variables d'environnement suivantes seront correctement
configurées pour correspondre aux valeurs pour se connecter à la base de
données :

  - `DB_DATABASE` : le  nom de la base de données

  - `DB_USERNAME` : le nom d'utilisateur de la base de données

  - `DB_PASSWORD` : le mot de passe de la base de données

Si vous n'utilisez pas MAMP, il faudra aussi commenter la ligne 49 du fichier
`config/database.php`, qui contient le contenu suivant :

```php
'unix_socket' => '/Applications/MAMP/tmp/mysql/mysql.sock',
```

Une fois tout ceci fait, tout ce qui concerne la configuration est OK, on peut
donc passer à l'installation, et lancer l'application.

Pour installer les différentes dépendances, il faut utiliser la commande
suivante : `composer install`

Une fois les dépendances installées, il faudra générer une clé d'application
avec cette commande : `php artisan key:generate`

Et pour lancer les migrations et les seeds, il faudra lancer la commande
suivante : `php artisan migrate --seed`

Dernière petite étape, on va créer un lien symbolique pour rendre accessible
les fichiers du stockage publique à tout le monde, c'est assez pratique pour
les photos uploadées par exemple^^ Cela se fait très rapidement avec la
commande suivante : `php artisan storage:link`

Normalement à ce point, tout le site devrait être correctement installé.



## Lancement et compilation

On peut donc lancer le serveur de développement, avec la commande :
`php artisan serve`, et il suffira de se rendre sur http://localhost:8000/
pour voir le site.

Les fichiers Sass et JavaScript sont compilés avec Laravel Mix. Pour que cela fonctionne il faut impérativement avoir installé node et les dépendances du projet avec `npm install`.
Pour compiler les assets : `npm run dev`
Pour compiler + minifier les assets : `npm run production`
Pour observer les changements : `npm run watch`
