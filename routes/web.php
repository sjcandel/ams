<?php

/*
|--------------------------------------------------------------------------
| MAIN
|--------------------------------------------------------------------------
*/

Route::get('/', 'Controller@home')->name('home');
Route::get('/club/presentation', 'Controller@presentation')->name('club.presentation');
Route::get('/club/equipe', 'Controller@equipe')->name('club.equipe');
Route::get('/horaires-tarifs/horaires', 'Controller@horaires')->name('horaires-tarifs.horaires');
Route::get('/horaires-tarifs/tarifs', 'Controller@tarifs')->name('horaires-tarifs.tarifs');
Route::get('/competitions/calendrier', 'Controller@calendrier')->name('competitions.calendrier');
Route::get('/competitions/resultats', 'Controller@resultats')->name('competitions.resultats');
Route::get('/galerie', 'Controller@galerie')->name('galerie');
Route::get('/galerie/{galerie}', 'Controller@galerieDetails')->name('galerieDetails');


/*
|--------------------------------------------------------------------------
| INTRANET
|--------------------------------------------------------------------------
*/

Auth::routes();

Route::prefix('intranet')->middleware('role:superadministrator|administrator|moderator|secretary|trainer|accountant|contributor|guest')->group(function(){
    Route::get('/', 'IntraController@index')->name('intranet');
    Route::get('dashboard', 'IntraController@dashboard')->name('intranet.dashboard');

    Route::get('/profil', 'ProfileController@showProfile')->name('showProfile');
    Route::get('/profil/edit','ProfileController@showChangePasswordForm')->name('editProfile');
    Route::post('/profil/edit','ProfileController@changePassword')->name('updateProfile');

    Route::get('/actualites/{actualite}/delete', 'ActualiteController@delete')->name('actualites.delete');

    Route::get('/judokas/{judoka}/unarchive', 'JudokaController@unarchive')->name('judokas.unarchive');
    Route::get('/judokas/{judoka}/pdf', 'JudokaController@pdf')->name('judokas.pdf');
    Route::resource('/judokas', 'JudokaController');

    Route::resource('/actualites', 'ActualiteController');
    Route::resource('/calendrier', 'CalendrierController');
    Route::resource('/resultats', 'ResultatController');
    Route::resource('/galerie', 'GalerieController');
    Route::resource('/fiches-de-frais', 'FraisController');

    Route::resource('/users', 'UserController');
    Route::resource('/permissions', 'PermissionController');
    Route::resource('/roles', 'RoleController');

    Route::get('/actualites/{actualite}/accept', 'ActualiteController@accept')->name('actualites.accept');
    Route::get('/actualites/{actualite}/reject', 'ActualiteController@reject')->name('actualites.reject');
    Route::get('/calendrier/{calendrier}/accept', 'CalendrierController@accept')->name('calendrier.accept');
    Route::get('/calendrier/{calendrier}/reject', 'CalendrierController@reject')->name('calendrier.reject');
});
